#include "sipm-board.h"

// USB-to-I2C Elite adapter
#include "USBtoI2C32.h"

// for Sleep() function
#include <windows.h>

// for _ftime() function
// http://msdn.microsoft.com/en-us/library/z54t9z5f.aspx
#include <sys/timeb.h>

// return current time with millisecond resolution
double time0()
{
  _timeb buf;
  _ftime(&buf);
  
  const double t = buf.time + buf.millitm / 1000.;
  return t;
}

using namespace std;

SiPM_Board::SiPM_Board()
: init_ok(false),
  profile(0)
{
  dac_state.reset(time0());
  
  // sometimes I2C startup do not succeeded on the first attempt
  for (int i = 0; i < 3; ++i) {
    init_ok = start_I2C_adapter();
    if (init_ok) break;
    Sleep(100); // delay 100 ms
  }
  
  if (init_ok) {
    init_ok = start_Board();
  }
}

SiPM_Board::~SiPM_Board()
{
  if (init_ok) stop_Board();
  stop_I2C_adapter();
}

bool SiPM_Board::start_I2C_adapter()
{
  const int dllver = Get_DLL_Version();
  log << "DLL Version = " << dllver << "\n";
  
  const int ndev = GetNumberOfDevices();
  log << "Number of adapters = " << ndev << "\n";
  
  if (ndev == 0) {
    log << "ERROR: No adapters connected" << "\n";
    return false;
  }
  
  int ret; // return code of I2C adapter calls
  
  int* serials = new int[ndev];
  const int nser = GetSerialNumbers(serials);
  log << "Serials number = " << nser << "\n";
  log << "\n";
  
  // TODO: check ndev vs. nser
  
  // enumerate all attached adapters
  for (int i = 0; i < ndev; ++i) {
    const int serial = serials[i];
    const int selected = SelectBySerialNumber(serial);
    log << "Adapter #" << serial << " selected = " << selected << "\n";
    
    const unsigned char hwrev = GetFirmwareRevision();
    log << "FirmwareRevision = " << (hwrev >> 4) << "." << (hwrev & 0xF) << "\n";
    
    unsigned char hwdata[3] = {0};
    ret = GetHardwareInfo(hwdata);
    log << "HW Info return code = " << ret << "\n"
          << "I2C frequency = " << (int) 2*hwdata[0] << "\n"
          << "3.3V Power Output State = " << (int) hwdata[1] << "\n"
          << "5V Power Output State = " << (int) hwdata[2] << "\n"
          << "\n";
  }
  
  if (nser == 0) {
    log << "ERROR: No adapters connected" << "\n";
    return false;
  }
  
  // select 1st adapter
  log << "Selecting first adapter to work with SiPM Board" << "\n";
  
  const int serial = serials[0];
  const int selected = SelectBySerialNumber(serial);
  log << "Adapter #" << serial << " selected = " << selected << "\n";
  
  const int busrecov = I2C_BusRecovery();
  log << "Recovering I2C bus, result = " << busrecov << "\n";
  
  const int newfreq = I2C_SetFrequency(I2C_Frequency);
  log << "Setting I2C frequency to " << (I2C_Frequency / 1000) << " kHz ...\n";
  //      << "Actual frequency " << (newfreq / 1000) << " kHz" << "\n";
  
  const int freq = I2C_GetFrequency();
  log << "Actual I2C frequency is " << (freq / 1000) << " kHz" << "\n";
  
  log << "\n";
  
  return true;
}

bool SiPM_Board::stop_I2C_adapter()
{
  ShutdownProcedure();
  return true;
}

bool SiPM_Board::start_Board()
{
  log << "Initializing SiPM control board..." << "\n";
  
  int ret;
  
  ret = I2C_WriteByte(TEMP_MCP9801, 0x01, TEMP_MCP9801_INIT);
  log << "Initializing TEMP_MCP9801 (Set resolution to 12 bit)... " << ret << "\n";
  if (ret != 0) return false;
  
  ret = I2C_SendByte(ADC_ADS7828, ADC_ADS7828_INIT);
  log << "Initializing ADC_ADS7828 (Internal Voltage Reference ON, A/D Converter ON)... " << ret << "\n";
  if (ret != 0) return false;
  
  // reset DAC_AD5383
  unsigned char data0[2] = {0, 0};
  ret = I2C_WriteArray(DAC_AD5383, 0x0F, 2, data0);
  log << "Initializing DAC_AD5383 (Soft RESET)... " << ret << "\n";
  if (ret != 0) return false;
  dac_state.reset(time0());
  
  // setup DAC_AD5383
  unsigned char data[2] = {DAC_AD5383_INIT[0], DAC_AD5383_INIT[1]};
  ret = I2C_WriteArray(DAC_AD5383, 0x0C, 2, data);
  log << "Initializing DAC_AD5383 (Internal Voltage Reference ON)... " << ret << "\n";
  if (ret != 0) return false;
  
  // select temperature function of SHT21
  log << "Selecting temperature function of SHT21... " << "\n";
  ret = DAC_AD5383_set_channel(DAC_CH_SHT21, 0) ? 0 : 1;
  if (ret != 0) return false;
  
  log << "Board initialization completed successfully" << "\n";
  
  return true;
}

// TODO: set all DAC channels to 0
bool SiPM_Board::stop_Board()
{
  setPixelsBV(0.);
  setPeltierV(0.);
  return true;
}

//bool SiPM_Board::readADC() {}

//bool SiPM_Board::writeDAC() {}

bool SiPM_Board::DAC_AD5383_set_channel(const unsigned char channel, const unsigned short int data)
{
  // check parameters
  if (channel > 31) return false; // channels: 0 - 31
  if (data > 4095) return false; // data: 0 - 4095
  
  // prepare output buffer
  const unsigned short int code = (data << 2) | 0xC000;
  const unsigned char code1 = code >> 8;
  const unsigned char code0 = code & 0xFF;
  unsigned char buffer[2*1] = {code1, code0};
  
  // send data
  const int ret = I2C_WriteArray(DAC_AD5383, channel, 2*1, buffer);
  log << "Setting DAC ch. #" << (int) channel << " to " << data << ", ret = " << ret << "\n";
  
  return (ret == 0);
}

bool SiPM_Board::DAC_AD5383_set_channels(const vector<unsigned short int>& data)
{
  // check parameters
  const size_t n = data.size();
  if (n > 32) return false; // channels: 0 - 31
  if (n == 0) return true;
  
  // prepare output buffer
  unsigned char buffer[2*32] = {0};  // 32 channels (64 bytes) max
  for (size_t i = 0; i < n; ++i) {
    if (data[i] > 4095) return false; // data: 0 - 4095
    
    const unsigned short int code = (data[i] << 2) | 0xC000;
    buffer[2*i + 0] = (code >> 8);
    buffer[2*i + 1] = (code & 0xFF);
  }
  
  // send data
  const int ret = I2C_WriteArray(DAC_AD5383, 0xFF, 2*n, buffer);
  log << "Setting DAC ch. #0 - #" << n << " to XXX, ret = " << ret << "\n";
  
  return (ret == 0);
}

bool SiPM_Board::MUX_ADG728_select_channel(const unsigned char mux, const unsigned char channel)
{
  if (mux > 3) return false;     // mux number: 0 - 3
  if (channel > 7) return false; // channel number: 0 - 7
  
  const int ret = I2C_SendByte(MUX_ADG728[mux], (1 << channel));
  
  log << "Selecting MUX #" << (int) mux << " ch. " << (int) channel << " ... " << ret << "\n";
  
  return (ret == 0);
}

// get ADC data
bool SiPM_Board::ADC_ADS7828_read_channel(const unsigned char channel, unsigned short int& data)
{
  if (channel > 7) return false; // ADC channels: 0 - 7
  
  unsigned char buffer[2] = {0};
  const int ret = I2C_ReadArray(ADC_ADS7828, ADC_ADS7828_CH[channel], 2, buffer);
  data = (ret == 0) ? (buffer[0] << 8) + buffer[1] : 0;
  
  //log << c.name << " (ADC ch. " << c.chADC << ") value = " << value << " " << c.unit << " (" << digital << ") " << ret << "\n";
  
  return (ret == 0);
}

bool SiPM_Board::TEMP_MCP9801_read(unsigned short int& data)
{
  unsigned char buffer[2] = {0};
  const int ret = I2C_ReadArray(TEMP_MCP9801, 0x00, 2, buffer);
  data = (ret == 0) ? (buffer[0] << 8) + buffer[1] : 0;
  
  //log << "Temperature = " << value << " C (raw data " << (int) data[0] << " " << (int) data[1] << ") ret = " << ret << "\n";
  
  return (ret == 0);
}

bool SiPM_Board::ADC_ADC081C021_read(unsigned short int& data)
{
  data = 0;
  
  log << "DUMMYADC readout is not implemented\n";
  
  return true;
}

bool SiPM_Board::readChannel(const unsigned char channel, InChannelValue& val, const size_t nloops)
{
  if (channel >= N_INPUT_CHANNELS) return false;
  
  const InChannel& c = InputChannels[channel];
  
  unsigned short int digital = 0;
  double value = 0.;
  
  // read ADC
  if ((c.path == MUXADC) || (c.path == ADC)) {
    // setup MUX
    if (c.path == MUXADC) {
      const bool ret = MUX_ADG728_select_channel(c.nMUX, c.chMUX);
      if (!ret) return false;
      
      Sleep(10); // delay 10 ms to recharge multiplexer
    }
    
    // get ADC data
    for (size_t i = 0; i < nloops; ++i) {
      const bool ret = ADC_ADS7828_read_channel(c.chADC, digital);
      const double i_scale = (channel < N_PIXELS) ? profile->max_current : 1;
      value += i_scale * c.scale * digital;
      
      log << c.name << " (ADC ch. " << c.chADC << ") value = " << value << " " << c.unit << " (" << digital << ") " << ret << "\n";
      if (!ret) return false;
    }
  }
  
  // read TEMP
  if (c.path == TEMP) {
    for (size_t i = 0; i < nloops; ++i) {
      const int ret = TEMP_MCP9801_read(digital);
      value += c.scale * (short int) digital;
      log << "Temperature = " << value << " C (raw data " << digital << ") ret = " << ret << "\n";
      if (!ret) return false;
    }
  }
  
  // read dummy ADC
  if (c.path == DUMMYADC) {
    const bool ret = ADC_ADC081C021_read(digital);
    value = digital;
  }
  
  val.channel = &c;
  val.data = digital;
  val.value = value / nloops;
  
  return true;
}

bool SiPM_Board::SHT21_switch_function()
{
  bool ret;
  
  // switch function of SHT21
  if (dac_state.sht21_rh) {
    // select temperature function of SHT21
    ret = DAC_AD5383_set_channel(DAC_CH_SHT21, 0);
    if (ret) dac_state.sht21_rh = false;
  }
  else {
    // select humidity function of SHT21
    ret = DAC_AD5383_set_channel(DAC_CH_SHT21, 0xFFF);
    if (ret) dac_state.sht21_rh = true;
  }
  
  return ret;
}


// readout all available channels on the board
InData SiPM_Board::read(const size_t nloops)
{
  vector<InChannelValue> voltages;
  
  for (int i = 0; i < N_INPUT_CHANNELS; ++i) {
    InChannelValue ch_val;
    
    const bool ret = readChannel(i, ch_val, nloops);
    if (!ret) {
      const InData r = {time0(), vector<InChannelValue>()};
      return r;
    }
    
    voltages.push_back(ch_val);
  }
  
  // correct temp and humidity (SHT21S sensor)
  const double sht21 = voltages[ADC_CH_MB_TEMP].value;
  const double Vdd = voltages[ADC_CH_VDD].value;
  const bool last = (! in_last.adc.empty());
  
  if (dac_state.sht21_rh) {
    voltages[ADC_CH_MB_TEMP].value = last ? in_last.adc[ADC_CH_MB_TEMP].value : 0;
    voltages[ADC_CH_MB_RH].value   =     -6 +    125 * (sht21 / Vdd);
  }
  else {
    voltages[ADC_CH_MB_TEMP].value = -46.85 + 175.72 * (sht21 / Vdd);
    voltages[ADC_CH_MB_RH].value   = last ? in_last.adc[ADC_CH_MB_RH].value : 0;
  }
  
  // calculate TEMP_RTD
  const double Vrtd = voltages[ADC_CH_MB_TEMP_RTD].value;
  voltages[ADC_CH_MB_TEMP_RTD].value = 519.5 * (1 - Vdd/2. / Vrtd);
  
  // correct pixels currents
  // I_sipm = I_measured - Vbv/6.4Mohm (12.1 uA at full voltage of 77.5V)
  for (size_t i = 0; i < N_PIXELS; ++i) {
    voltages[i].value -= dac_state.bv[i]/6.4;
  }
  
  SHT21_switch_function();
  
  const InData r = {time0(), voltages};
  in_last = r;
  return r;
}

// prepare 12 bit data word for DAC based on floating-point <value> and max <range>
unsigned short int voltage_to_data12(const double value, const double range)
{
  const double data = (value / range) * 4096 + 0.5;
  
  // check overflow
  if (data > 4095) return 0x0FFF;
  
  // check underflow
  if (data < 0) return 0x0000;
  
  // data in 12 bit range
  return data;
}

double data12_to_voltage(const unsigned short int data, const double range)
{
  return data / 4096. * range;
}




// set Bias Voltage of Pixels
OutData SiPM_Board::setPixelsBV(const vector<double>& bv)
{
  if (bv.size() != N_PIXELS) {
    OutData od = dac_state;
    od.ok = false;
    return od;
  }
  
  while (true) {
    bool ramp_loop = false;
    
    // prepare data block
    for (size_t i = 0; i < bv.size(); ++i) {
      bool ramp_flag = ((bv[i] - dac_state.bv[i]) > BV_RAMP);
      const double v = ramp_flag ? (dac_state.bv[i] + BV_RAMP) : bv[i];
      
      dac_state.bv_data[i] = profile->bv_to_data(i, v);
      dac_state.bv[i] = profile->data_to_bv(i, dac_state.bv_data[i]);
      
      ramp_flag = ramp_flag ? (dac_state.bv_data[i] < 4095) : false;
      ramp_loop = ramp_flag || ramp_loop;
    }
    
    dac_state.time = time0();
    dac_state.ok = DAC_AD5383_set_channels(dac_state.bv_data);
    
    if (ramp_loop) Sleep(BV_RAMP_DELAY);
    else break;
  }
  
  return dac_state;
}

OutData SiPM_Board::setPixelsBV(const double BV)
{
  //cout << "Setting BV of all Pixels (DAC ch. #0 - #17) to " << BV << " V ... " << ret << endl;
  const OutData& od = setPixelsBV(vector<double>(N_PIXELS, BV));
  return od;
}

// set Voltage of Peltier Element
OutData SiPM_Board::setPeltierV(const double voltage)
{
  dac_state.time = time0();
  
  dac_state.pv_data = voltage_to_data12(voltage, PELTIER_RANGE);
  dac_state.pv = data12_to_voltage(dac_state.pv_data, PELTIER_RANGE);
  
  const bool ret = DAC_AD5383_set_channel(DAC_CH_PELTIER, dac_state.pv_data);
  log << "Setting voltage of Peltier Element (DAC ch. #" << DAC_CH_PELTIER << ") to " << voltage << " V ... " << ret << "\n";
  
  dac_state.ok = ret;
  
  return dac_state;
}

std::string SiPM_Board::cleanLog()
{
  const string buf = log.str();
  log.str(string());
  return buf;
}
