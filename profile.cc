#include "profile.h"

#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
using namespace std;

// convert string to value
template<typename T>
T cast(const string& s)
{
  istringstream iss(s);
  T x;
  iss >> x;
  return x;
}

string strip(string s)
{
  // remove leading spaces
  const size_t pos0 = s.find_first_not_of(" ");
  if (pos0 == string::npos) return "";
  s.erase(0, pos0);
  
  // remove trailing spaces
  const size_t pos1 = s.find_last_not_of(" ");
  if (pos1 == string::npos) return "";
  s.erase(pos1 + 1);
  
  // replace: "  " -> " "
  while (true) {
    const size_t pos = s.find("  ");
    if (pos == string::npos) break;
    
    s.erase(pos, 1);
  }
  
  return s;
}

vector<string> split(string s, const string& sep)
{
  vector<string> v;
  
  while (true) {
    const size_t pos = s.find_first_of(sep);
    if (pos == string::npos) break;
    
    v.push_back(s.substr(0, pos));
    s.erase(0, pos + 1);
  }
  
  v.push_back(s);
  return v;
}

Profile::Profile(const char* fname)
{
  read(fname);
  calc_bv_calibration();
}

void Profile::reset()
{
  bias_voltage_range = 0;
  max_current = 0;
  dac_cal.clear();
  dac_cal.resize(24);
  
  calibration.clear();
}

bool Profile::read(const char* fname)
{
  reset();
  
  ifstream f(fname);
  if (!f) return false;
  
  string line, section;
  
  // read file line by line
  while (getline(f, line)) {
    // detect new section
    const vector<string>& sect = split(line, "[]");
    
    if (sect.size() >= 3) {
      // extract section name
      section = strip(sect[1]);
      continue;
    }
    
    // skip comments
    const size_t hash_pos = line.find("#");
    if (hash_pos != string::npos) continue;
    
    // detect property
    const vector<string>& prop = split(line, "=");
    if (prop.size() != 2) continue;
    
    // extract "key = value" of the property string
    const string key = strip(prop[0]);
    const string value = strip(prop[1]);
    
    if (section == "general") {
      if (key == "bias_voltage_range") bias_voltage_range  = cast<double>(value);
      if (key == "max_current")        max_current         = cast<double>(value);
    }
    else if (section == "bias_voltage_calibration") {
      const int ch = cast<int>(key) - 1;
      if ((ch < 0) || ((ch + 1) > dac_cal.size())) continue;
      
      const vector<string>& values = split(value, " ");
      
      for (size_t i = 0; i < values.size(); ++i) {
        const vector<string>& point = split(values[i], ":");
        if (point.size() != 2) continue;
        
        const DAC_point p = {cast<unsigned short int>(point[0]), cast<double>(point[1])};
        dac_cal[ch].push_back(p);
      }
    }
  }
  
  return true;
}

void Profile::calc_bv_calibration()
{
  const size_t n = dac_cal.size();
  calibration.resize(n);
  
  for (size_t i = 0; i < n; ++i) {
    const size_t m = dac_cal[i].size();
    
    if (m < 2) {
      const Calibration_point p = {bias_voltage_range / 4096, 0, 1, false};
      calibration[i] = p;
      continue;
    }
    
    double Sx  = 0;
    double Sx2 = 0;
    double Sy  = 0;
    double Sy2 = 0;
    double Sxy = 0;
    
    for (size_t j = 0; j < m; ++j) {
      const DAC_point& p = dac_cal[i][j];
      const double x = p.data;
      const double y = p.voltage;
      
      Sx  += x;
      Sx2 += x * x;
      Sy  += y;
      Sy2 += y * y;
      Sxy += x * y;
    }
    
    Sx  /= m;
    Sx2 /= m;
    Sy  /= m;
    Sy2 /= m;
    Sxy /= m;
    
    const double slope = (Sxy - Sx*Sy) / (Sx2 - Sx*Sx);
    const double intercept = Sy - slope*Sx;
    const double rsq = (Sxy - Sx*Sy) * (Sxy - Sx*Sy) / (Sx2 - Sx*Sx) / (Sy2 - Sy*Sy);
    const Calibration_point p = {slope, intercept, rsq, true};
    calibration[i] = p;
  }
}

unsigned short int Profile::bv_to_data(const unsigned short channel, const double bv) const
{
  const Calibration_point& p = calibration[channel];
  const double data = (bv - p.intercept) / p.slope + 0.5;
  
  // check overflow
  if (data > 4095) return 4095;
  
  // check underflow
  if (data < 0) return 0;
  
  // data in 12 bit range
  return data;
}

double Profile::data_to_bv(const unsigned short channel, const unsigned short int data) const
{
  const Calibration_point& p = calibration[channel];
  return data * p.slope + p.intercept;
}

std::ostream& operator<<(std::ostream& os, const Profile& p)
{
  os << "Configuration:\n"
      << "  bias_voltage_range = " << p.bias_voltage_range << " V\n"
      << "  max_current = " << p.max_current << " uA\n"
      << "\n";
  
  os << "Bias voltage calibration data:\n";
  
  for (size_t i = 0; i < p.dac_cal.size(); ++i) {
    const Calibration_point& cp = p.calibration[i];
    
    os << "  ch" << (i + 1) << " = ";
    
    if (cp.isCalibrated) {
      for (size_t j = 0; j < p.dac_cal[i].size(); ++j) {
        const DAC_point& dp = p.dac_cal[i][j];
        os << dp.data << ":" << dp.voltage << "  ";
      }
    }
    else {
      os << "uncalibrated ";
    }
    
    os << "|  slope = " << cp.slope << "  intercept = " << cp.intercept << "  rsq = " << cp.rsq << "\n";
    
    // empty line between groups
    if ((i % 8) == 7) os << "\n";
  }
  
  return os;
}
