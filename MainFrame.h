#ifndef MainFrame_h
#define MainFrame_h

// SiPM Board Interface
#include "sipm-board.h"

// ROOT
#include <TGFrame.h>
#include <TRootEmbeddedCanvas.h>
//#include <TGSlider.h>
#include <TGStatusBar.h>
#include <TGNumberEntry.h>
#include <TGLabel.h>
#include <TGFileDialog.h>
#include <TGTab.h>
#include <TTimer.h>
#include <TMultiGraph.h>

// c++
#include <deque>

class MainFrame: public TGMainFrame {
public:
  MainFrame(const TGWindow* p, UInt_t w, UInt_t h);
  ~MainFrame();

  void DoLoadProfile(Int_t id);
  
  void DoEntryDacChanged();
  void DoBoardRead();
  void DoBoardSetPixels();
  void DoBoardSetAllPixels();
  void DoBoardSetAllPixelsZero();
  void DoBiasVoltageSave();
  void DoBiasVoltageLoad();
  void DoBoardShiftPixels();
  void DoBoardSetPeltier();
  void DoAutoRead(Bool_t on);
  
  void DoDisplayReadoutData();
  
  void DoLoadPreset(Int_t id);
  void DoSetTimerDelay();
  void DoSaveDAQData();
  void DoClearDAQData();
  
  void DoIVRun();
  void DoDisplayIVData();
  void DoSetPixelsBreakdown();
  void DoSaveIVData();
  void DoIVRangesSave();
  void DoIVRangesLoad();
  
  void DoMenu(Int_t id);
  void HelpAbout();
  void CloseWindow();
  
  ClassDef(MainFrame, 0)

private:
  // hardware tab
  TGLabel* labelHardware;
  
  // board profile tab
  TGComboBox* comboProfile;
  TGLabel* labelProfile;
  
  // DAC/ADC tab
  TGNumberEntryField* entryDAC[N_PIXELS];
  TGNumberEntryField* entryAllPixels;
  TGTextButton* buttonSetAll;
  TGTextEntry* entryADC[N_INPUT_CHANNELS];
  TGNumberEntryField* entryVoltageShift;
  
  // peltier
  TGCheckButton* checkMBTAuto;
  TGNumberEntryField* entryMBT;
  TGLabel* labelDeviation;
  TGNumberEntryField* entryPCV;
  
  // graphs tab
  TRootEmbeddedCanvas* canvasADCReadout;
  TGButtonGroup* groupChannels;
  TGComboBox* comboDisplayType;
  TGCheckButton* checkShowLegend;
  
  // DAQ tab
  TGTabElement* tabDAQhead;
  TGCheckButton* checkAuto;
  TGNumberEntry* numberReadLoops;
  TGNumberEntryField* numberHistoryLen;
  TGNumberEntry* numberDelay;
  TGLabel* labelBufferSize;
  
  // IV tab
  TGTabElement* tabIvScanHead;
  TRootEmbeddedCanvas* canvasIVRun;
  enum {IVRanges = 6};
  TGNumberEntryField* entryRange[IVRanges];
  TGNumberEntry* step_delay;
  TGComboBox* comboDisplay;
  TGComboBox* comboChGroup;
  TGCheckButton* checkVSLegend;
  TGLabel* labelVbreakdown;
  
  TGStatusBar* statusBar;
  
  TTimer timer;
  
  enum {idFileOpen, idFileExit, idHelpAbout};
  enum {idIV, iddIdV};
  enum {idChAll, idCh1_8, idCh9_16, idCh17_24};
  enum {idAll, idPixels, idPixels1_8, idPixels9_16, idPixels16_24, idNonPixels, idPeltier, idEnvironment, idNone};
  enum {idGraph, idHistogram};
  enum DACStatus {idChanged, idSuccess, idFail};
  
  void redraw(TGWindow* window);
  void setStatus(const char* status);
  void UpdateADCGraphs();
  void UpdateADCHistograms();
  void ControlPeltier();
  void setDacStatus(const DACStatus status);
  std::vector<double> listIvScanVoltages();
  
  SiPM_Board* board;
  std::deque<InData> history;
  std::deque<OutData> ohistory;
  std::vector<DataPoint> ivdata;
  
  std::vector<bool> getIVselection() const;
};

#endif
