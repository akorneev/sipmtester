   HO SiPM Board tester
   ====================

Dependencies:
-------------

* Install ROOT 6.20.00
  https://root.cern.ch/download/root_v6.20.00.win32.vc16.exe
  
  Select the option "Add ROOT to the system PATH for all users" during installation.

* Install driver for USB-to-I2C Elite adapter
  https://www.i2ctools.com/Downloads/USBtoI2Celite/v7/USBtoI2C_Elite_V7_Win32_Installation.zip

* Now you are ready to connect SiPM board and run the program.


Development instruction:
------------------------

* Install ROOT and adapter driver

* Install Visual Studio 2019 Build Tools
  https://visualstudio.microsoft.com/downloads/?q=build+tools

* Install GIT client
  https://git-scm.com/

* Open VS command prompt
  run "vs2019.lnk"

* Checkout / compile sources / run program
  > git clone https://gitlab.cern.ch/akorneev/sipmtester.git
  > cd sipmtester
  > nmake
  > sipm-tester.exe

* Build distributive:
  > nmake dist

Code repository:
----------------

  Access is available through web interface:
  https://gitlab.cern.ch/akorneev/sipmtester


Documentation:
--------------

* HO SiPM Board
  Electric Diagrams For HO SiPM Upgrade Mounting/Control/Bias Boards
  https://cms-docdb.cern.ch/cgi-bin/DocDB/ShowDocument?docid=6046
  
  Programming of the HO SiPM Control Board
  https://cms-docdb.cern.ch/cgi-bin/DocDB/ShowDocument?docid=3104

* USB-to-I2C Elite adapter
  https://www.i2ctools.com/manuals-and-downloads/
