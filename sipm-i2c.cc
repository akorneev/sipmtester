// SiPM Board Interface
#include "sipm-board.h"

// c/c++
#include <iostream>
#include <vector>
using namespace std;

//#include <windows.h>

int main(int argc, char** argv)
{
  cout << "HO SiPM Board I2C communication tester\n"
       << "builded on " __DATE__ " " __TIME__ "\n"
       << endl;
  
  SiPM_Board board;
  
  cout << board.cleanLog() << endl;
  
  if (!board) {
    cerr << "Failed to start HO SiPM Board" << endl;
    return 1;
  }
  
  cout << "Reading input channels..." << endl;
  const InData input = board.read(1);
  cout << "Time: " << fixed << input.time << endl;
  for (size_t i = 0; i < input.adc.size(); ++i) {
    cout << input.adc[i].channel->name << " = " << input.adc[i].value << " " << input.adc[i].channel->unit << endl;
  }
  
  cout << endl;
  
  // set Pixels BV
  const float BV = 75;
  cout << "Setting Pixels BV to " << BV << " V ... " << board.setPixelsBV(BV).bv[0] << endl;
  
  // set Peltier Element voltage
  const float PV = 2;
  cout << "Setting Peltier voltage to " << PV << " V ... " << board.setPeltierV(PV).pv << endl;
  
  return 0;
}
