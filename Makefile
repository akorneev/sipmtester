# c++ compiler options
# compile only
# suppresses the copyright notice
# use static multithread version of the run-time library
# use RTTI
# specifies the model of exception handling: catches C++ exceptions only and tells the compiler to assume that extern C functions never throw a C++ exception
# /Zi - add debug information (to be used together with /debug swich for link)
CXXFLAGS = /c /nologo /MT /GR /EHsc

# linker options
# suppresses the copyright notice
# Win32 character-mode (or GUI) application
# sybsystem version is specified explicitly to make compatibility with Windows XP
#  http://blogs.msdn.com/b/vcblog/archive/2012/10/08/10357555.aspx
# /debug - add debug information
LINKFLAGS = /nologo /subsystem:console,5.01
LINKFLAGS_GUI = /nologo /subsystem:windows,5.01

# includes and libs
ROOTSYS = c:\root_v6.20.00
INCFLAGS = /ISDK /I$(ROOTSYS)\include
LIBFLAGS = /libpath:SDK USBtoI2C32.lib /libpath:$(ROOTSYS)\lib libCore.lib libRIO.lib libHist.lib libGraf.lib libTree.lib libGui.lib libSpectrum.lib


all: sdk sipm-i2c.exe sipm-tester.exe test.exe

# prepare SDK for i2c adapter
sdk:
  md SDK
  copy "c:\Program Files (x86)\i2ctools\USB-to-I2C Elite\USBDriver\USBtoI2C32.dll" SDK
  copy "c:\Program Files (x86)\i2ctools\USB-to-I2C Elite\DLL Examples\VC 6\USBtoI2C32.h" SDK
  copy "c:\Program Files (x86)\i2ctools\USB-to-I2C Elite\DLL Examples\VC 6\USBtoI2C32.lib" SDK

# prototype #1
sipm-i2c.exe: sipm-i2c.cc sipm-board.cc sipm-board.h profile.cc profile.h
  cl $(CXXFLAGS) /ISDK sipm-i2c.cc sipm-board.cc profile.cc
  link $(LINKFLAGS) /libpath:SDK USBtoI2C32.lib sipm-i2c.obj sipm-board.obj profile.obj
  del *.obj

# board tester application
sipm-tester.exe: sipm-tester.cc MainFrame.cc MainFrame.h sipm-board.cc sipm-board.h profile.cc profile.h
  $(ROOTSYS)\bin\rootcint.exe -f MainFrameDict.cxx -c MainFrame.h MainFrameLinkDef.h
  cl $(CXXFLAGS) $(INCFLAGS) sipm-tester.cc MainFrame.cc MainFrameDict.cxx sipm-board.cc profile.cc
  link $(LINKFLAGS_GUI) $(LIBFLAGS) sipm-tester.obj MainFrame.obj MainFrameDict.obj sipm-board.obj profile.obj
  del *.obj
  del MainFrameDict.cxx MainFrameDict.h
  del sipm-tester.lib sipm-tester.exp

test.exe: test.cc profile.cc profile.h
  cl /EHsc test.cc profile.cc
  del *.obj

dist:
  mkdist.cmd

clean:
  del *.exe *.obj
  -rmdir /S /Q SDK
