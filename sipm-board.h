#ifndef sipm_board_h
#define sipm_board_h

#include <vector>
#include <sstream>

#include "profile.h"

enum ChannelPath {
  MUXADC,    // channel connected through analog switch (ADG728) to ADC (ADS7828)
  ADC,       // channel directly connected to ADC (ADS7828)
  TEMP,      // control board temperature sensor (MCP9801) channel
  DUMMYADC,  // dummy ADC (ADC081C021)
  DAC        // DAC (AD5383)
};

// channel description
struct InChannel {
  char* name;           // channel name
  ChannelPath path;     // cnannel connection path
  unsigned char nMUX;   // ADG728 switch number #0 - #3
  unsigned char chMUX;  // ADG728 switch channel #0 - #7
  unsigned char chADC;  // ADS7828 ADC channel
  
  double scale;         // ADC counts to value conversion factor
  char* unit;           // unit of value
};

struct InChannelValue {
  const InChannel* channel;
  unsigned short int data;
  double value;
};

// macro for array length calculation
#define DIM(a) (sizeof(a)/sizeof(a[0]))

// ADC input channels definition (path, MUX #, MUX ch., ADC ch.)
const InChannel InputChannels[] = {
// name              path     n ch  ch    scale     unit
  {"I_01",           MUXADC,  0, 0, 0,      1./4096, "uA"},
  {"I_02",           MUXADC,  0, 1, 0,      1./4096, "uA"},
  {"I_03",           MUXADC,  0, 2, 0,      1./4096, "uA"},
  {"I_04",           MUXADC,  0, 3, 0,      1./4096, "uA"},
  {"I_05",           MUXADC,  0, 4, 0,      1./4096, "uA"},
  {"I_06",           MUXADC,  0, 5, 0,      1./4096, "uA"},
  {"I_07",           MUXADC,  0, 6, 0,      1./4096, "uA"},
  {"I_08",           MUXADC,  0, 7, 0,      1./4096, "uA"},
  
  {"I_09",           MUXADC,  1, 0, 1,      1./4096, "uA"},
  {"I_10",           MUXADC,  1, 1, 1,      1./4096, "uA"},
  {"I_11",           MUXADC,  1, 2, 1,      1./4096, "uA"},
  {"I_12",           MUXADC,  1, 3, 1,      1./4096, "uA"},
  {"I_13",           MUXADC,  1, 4, 1,      1./4096, "uA"},
  {"I_14",           MUXADC,  1, 5, 1,      1./4096, "uA"},
  {"I_15",           MUXADC,  1, 6, 1,      1./4096, "uA"},
  {"I_16",           MUXADC,  1, 7, 1,      1./4096, "uA"},
  
  {"I_17",           MUXADC,  2, 0, 2,      1./4096, "uA"},
  {"I_18",           MUXADC,  2, 1, 2,      1./4096, "uA"},
  {"I_19",           MUXADC,  2, 2, 2,      1./4096, "uA"},
  {"I_20",           MUXADC,  2, 3, 2,      1./4096, "uA"},
  {"I_21",           MUXADC,  2, 4, 2,      1./4096, "uA"},
  {"I_22",           MUXADC,  2, 5, 2,      1./4096, "uA"},
  {"I_23",           MUXADC,  2, 6, 2,      1./4096, "uA"},
  {"I_24",           MUXADC,  2, 7, 2,      1./4096, "uA"},
  
  {"+V1_IN",         MUXADC,  3, 0, 3,   6*2.5/4096, "V"},
  {"+VDD",           MUXADC,  3, 1, 3,   2*2.5/4096, "V"},
  {"BV_FILTER",      MUXADC,  3, 2, 3,  79*2.5/4096, "V"},
  {"V_PELTIER+",     MUXADC,  3, 3, 3,   6*2.5/4096, "V"},
  {"I_PELTIER",      MUXADC,  3, 4, 3,     2.5/4096, "A"},
  {"MB_TEMP_RTD",    MUXADC,  3, 5, 3,     2.5/4096, "C"},
  
  // two channels share same physical path
  // the value of path determined by DAC_CH_SHT21 output channel
  // (low - temperature, high - relative humidity)
  {"MB_TEMP",        MUXADC,  3, 6, 3,     2.5/4096, "C"},
  {"MB_RH",          MUXADC,  3, 6, 3,     2.5/4096, "%"},
  
  {"DAC_MON_V",      MUXADC,  3, 7, 3,     2.5/4096, "V"},
  
  {"CB_TEMP",        TEMP,    0, 0, 0,     1./256,  "C"},
  
  {"DUMMY_ADC",     DUMMYADC, 0, 0, 0,     .0/4096, ""},
};

// I2C bus speed - 100 kHz
const int I2C_Frequency = 100000;

// I2C addresses of chips
const unsigned char TEMP_MCP9801 = 0x94; // temperature sensor
const unsigned char ADC_ADS7828 = 0x96; // 12 bit, 8 channels ADC
const unsigned char MUX_ADG728[4] = {0x98, 0x9A, 0x9C, 0x9E}; // 4 x (8-to-1 analog switch)
const unsigned char ADC_ADC081C021 = 0xA8; // 8 bit ADC, dummy
const unsigned char DAC_AD5383 = 0xAA; // 12 bit, 32 channels DAC

// MCP9801 resolution to 12 bit
const unsigned char TEMP_MCP9801_INIT = 0x60;

// ADS7828 Internal Voltage Reference ON, A/D Converter ON
const unsigned char ADC_ADS7828_INIT = 0x0C;
// ADS7828 channels codes
const unsigned char ADC_ADS7828_CH[] = {0x8C, 0xCC, 0x9C, 0xDC, 0xAC, 0xEC, 0xBC, 0xFC};
// ADS7828 Reference Voltage
const double ADC_ADS7828_RANGE = 2.5; // 2.5 V

// AD5383 Internal Voltage Reference ON (CR8 = 1)
const unsigned char DAC_AD5383_INIT[2] = {0x04, 0x00};
const double DAC_AD5383_RANGE = 1.25 * 2; // {DAC internal voltage reference} * {DAC output buffer ampifier}

//
const int N_INPUT_CHANNELS = DIM(InputChannels);
const int N_PIXELS = 24; // DAC ch. #0 - #23 (control bias voltage)
const int DAC_CH_PELTIER = 31; // DAC ch. #31 (control peltier voltage)
const int DAC_CH_SHT21 = 30; // DAC ch. #30 (select temperature/humidity function)
const double BV_RAMP = 1.; // maximum ramp-up voltage step
const int BV_RAMP_DELAY = 10; // ramp-up step delay, ms
const double PELTIER_RANGE = DAC_AD5383_RANGE * 2; // 5 V

const int ADC_CH_VDD = 25;
const int ADC_CH_MB_TEMP_RTD = 29;
const int ADC_CH_MB_TEMP = 30;
const int ADC_CH_MB_RH = 31;
const int ADC_CH_CB_TEMP = 33;
const int PELTIER_TEMP_SENSOR = ADC_CH_MB_TEMP_RTD;

struct InData {
  double time;
  std::vector<InChannelValue> adc;
};

struct OutData {
  double time;
  
  double bv[N_PIXELS]; // bias voltage
  std::vector<unsigned short int> bv_data;
  double pv;           // peltier voltage
  unsigned short int pv_data;
  bool sht21_rh;
  
  bool ok; // true if DAC set operation completed succesfully
  
  //operator bool() const {return ok;}
  
  void reset(const double& time0)
  {
    time = time0;
    pv = 0.;
    pv_data = 0;
    sht21_rh = false;
    bv_data.resize(N_PIXELS);
    
    for (size_t i = 0; i < N_PIXELS; ++i) {
      bv_data[i] = 0;
      bv[i] = 0.;
    }
    
    ok = true;
  }
};

struct DataPoint {
  OutData out;
  InData in;
};

class SiPM_Board {
private:
  bool init_ok;
  std::ostringstream log;
  
  OutData dac_state;
  InData in_last;
  
  bool start_I2C_adapter();
  bool stop_I2C_adapter();
  bool start_Board();
  bool stop_Board();
  
  bool DAC_AD5383_set_channel(const unsigned char channel, const unsigned short int data);
  bool DAC_AD5383_set_channels(const std::vector<unsigned short int>& data);
  bool ADC_ADS7828_read_channel(const unsigned char channel, unsigned short int& data);
  bool MUX_ADG728_select_channel(const unsigned char mux, const unsigned char channel);
  bool TEMP_MCP9801_read(unsigned short int& data);
  bool ADC_ADC081C021_read(unsigned short int& data);
  bool SHT21_switch_function();
  
  bool readChannel(const unsigned char channel, InChannelValue& val, const size_t nloops);
  
  //bool readADC();
  //bool writeDAC();
  
public:
  SiPM_Board();
  ~SiPM_Board();
  
  const Profile* profile;
  
  operator bool() const {return init_ok;}
  
  InData read(const size_t nloops);
  
  OutData getOutState() const { return dac_state; };
  OutData setPixelsBV(const std::vector<double>& voltage);
  OutData setPixelsBV(const double voltage);
  OutData setPeltierV(const double voltage);
  
  std::string cleanLog();
};

#endif
