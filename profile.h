#pragma once

#include <vector>
#include <ostream>

struct DAC_point {
  unsigned short int data;
  double voltage;
};

struct Calibration_point {
  double slope;
  double intercept;
  double rsq;
  bool isCalibrated;
};

class Profile {
private:
  bool read(const char* fname);
  void calc_bv_calibration();
  void reset();
  
public:
  double bias_voltage_range;
  double max_current;
  
  std::vector<std::vector<DAC_point> > dac_cal;
  std::vector<Calibration_point> calibration;
  
  Profile(const char* fname);
  
  unsigned short int bv_to_data(const unsigned short channel, const double bv) const;
  double data_to_bv(const unsigned short channel, const unsigned short int data) const;
};

std::ostream& operator<<(std::ostream& os, const Profile& p);
