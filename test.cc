#include "profile.h"

#include<iostream>
using namespace std;

int main()
{
  const char* fname[2] = {"HE-low.profile", "HE-high.profile"};
  
  for (size_t j = 0; j < 2; ++j) {
    cout << "Profile = " << fname[j] << endl;
    
    const Profile pr(fname[j]);
    
    cout << "bias_voltage_range = " << pr.bias_voltage_range << endl;
    cout << "max_current = " << pr.max_current << endl;
    
    for (size_t i = 0; i < pr.dac_cal.size(); ++i) {
      cout << "ch#" << (i + 1) << " = ";
      
      const Calibration_point& cp = pr.calibration[i];
      
      if (cp.isCalibrated) {
        for (size_t j = 0; j < pr.dac_cal[i].size(); ++j) {
          const DAC_point& dp = pr.dac_cal[i][j];
          cout << dp.data << ":" << dp.voltage << "  ";
        }
      }
      else
        cout << "uncalibrated ";
      
      cout << "|  slope = " << cp.slope << "  intercept = " << cp.intercept << "  rsq = " << cp.rsq
           << "  | data(20 volt) = " << pr.bv_to_data(i, 20)
           << "  | bv(1000) = " << pr.data_to_bv(i, 1000)
           << endl;
    }
    
    cout << endl;
  }
  
  return 0;
}
