#include "MainFrame.h"
#include "profile.h"

#include <TApplication.h>
#include <TGClient.h>
#include <TGButton.h>
#include <TGTab.h>
#include <TGMenu.h>
#include <TGMsgBox.h>
#include <TGButtonGroup.h>
#include <TG3DLine.h>
#include <TGComboBox.h>

#include <TROOT.h>
#include <TSystem.h>
#include <RVersion.h>

#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <TStyle.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TLegend.h>
#include <THStack.h>
#include <TLine.h>

#include <TFile.h>
#include <TTree.h>
#include <TSpectrum.h>

#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <float.h>
using namespace std;

MainFrame::MainFrame(const TGWindow* p, UInt_t w, UInt_t h)
: TGMainFrame(p, w, h),
  board(0)
{
  /// --- set default styles of plots ---
  //gROOT->SetStyle("Plain");
  //gStyle->SetPalette(1, 0);
  //gStyle->SetOptStat("emrou");
  //gStyle->SetStatFormat("7.3g");
  //gStyle->SetStatFontSize(0.1);
  //gStyle->SetStatW(0.3);
  //gStyle->SetCanvasBorderMode(0);
  //gStyle->SetCanvasColor(10);
  //gStyle->SetPadBorderMode(0);
  //gStyle->SetPadColor(33);
  //gStyle->SetFillStyle(1001);
  
  SetWindowName(gROOT->GetName());
  Move(10, 35);
  
  // --- main layout ----------------------------
  TGTab* tabs;
  {
    // main menu
    TGMenuBar* menu = new TGMenuBar(this, 0, 0, 0);
    AddFrame(menu);
    
      TGPopupMenu* fileMenu = menu->AddPopup("&File");
      fileMenu->Connect("Activated(Int_t)", "MainFrame", this, "DoMenu(Int_t)");
      //fileMenu->AddEntry("&Open...", idFileOpen);
      fileMenu->AddEntry("&Exit", idFileExit);
      
      TGPopupMenu* helpMenu = menu->AddPopup("&Help");
      helpMenu->Connect("Activated(Int_t)", "MainFrame", this, "DoMenu(Int_t)");
      helpMenu->AddEntry("&About", idHelpAbout);
    
    // tabs
    tabs = new TGTab(this);
    AddFrame(tabs, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
    
    // status bar
    statusBar = new TGStatusBar(this);
    AddFrame(statusBar, new TGLayoutHints(kLHintsExpandX, 0, 0, 2));
  }
  
  TGLayoutHints* hints = new TGLayoutHints(kLHintsNormal, 2, 2, 2, 2);
  TGLayoutHints* hintsLabel = new TGLayoutHints(kLHintsCenterY, 2, 2, 2, 2);
  
  // "Hardware" tab
  {
    TGCompositeFrame* tabProfile = tabs->AddTab("Hardware");
      
      // hardware information
      labelHardware = new TGLabel(tabProfile);
      labelHardware->SetTextJustify(kTextLeft | kTextTop);
      tabProfile->AddFrame(labelHardware, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
  }
  
  // "Board profile" tab
  {
    TGCompositeFrame* tabProfile = tabs->AddTab("Board profile");
    
      // presets selection
      comboProfile = new TGComboBox(tabProfile);
      comboProfile->AddEntry("HE-low.profile", 0);
      comboProfile->AddEntry("HE-high.profile", 1);
      comboProfile->AddEntry("uncalibrated.profile", 2);
      comboProfile->Resize(150, 20);
      comboProfile->Connect("Selected(Int_t)", "MainFrame", this, "DoLoadProfile(Int_t)");
      tabProfile->AddFrame(comboProfile, hints);
      
      // profile details information
      labelProfile = new TGLabel(tabProfile);
      labelProfile->SetTextJustify(kTextLeft | kTextTop);
      tabProfile->AddFrame(labelProfile, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
  }
  
  // "Control and readout" tab
  {
    TGCompositeFrame* tabDACADC = tabs->AddTab("Control and readout");
    tabDACADC->ChangeOptions((tabDACADC->GetOptions() & ~kVerticalFrame) | kHorizontalFrame);
    
      // DAC Set group
      TGGroupFrame* group = new TGGroupFrame(tabDACADC, "Bias Voltage Control");
      //group->SetLayoutManager(new TGMatrixLayout(group, 0, 2));
      tabDACADC->AddFrame(group, hints);
      
        // bias voltage entry fields
        TGVerticalFrame* frameVolt = new TGVerticalFrame(group);
        frameVolt->SetLayoutManager(new TGMatrixLayout(frameVolt, 0, 2));
        group->AddFrame(frameVolt, hints);
          
          for (size_t i = 0; i < N_PIXELS; ++i) {
            // replace "leak" -> "BV" is a workaround
            // TODO: implement dedicated OutputChannels list with channels descriptions
            TString name = InputChannels[i].name;
            name.ReplaceAll("I_", "BV_");
            
            TGLabel* label = new TGLabel(frameVolt, name);
            frameVolt->AddFrame(label, hints);
            
            entryDAC[i] = new TGNumberEntryField(frameVolt, -1, 0, TGNumberFormat::kNESReal);
            entryDAC[i]->Connect("TextChanged(char*)", "MainFrame", this, "DoEntryDacChanged()");
            entryDAC[i]->Connect("ReturnPressed()", "MainFrame", this, "DoBoardSetPixels()");
            frameVolt->AddFrame(entryDAC[i], hints);
          }
          
          // add <TAB> navigation between bias voltage fields
          for (size_t i = 0; i < N_PIXELS; ++i) {
            const size_t nexti = (i + 1) % N_PIXELS;
            entryDAC[i]->Connect("TabPressed()", "TGNumberEntryField", entryDAC[nexti], "SetFocus()");
          }
        
        // bias voltage controls
        TGHorizontalFrame* frameVoltCtr = new TGHorizontalFrame(group);
        group->AddFrame(frameVoltCtr, new TGLayoutHints(kLHintsExpandX));
          
          // spacer
          frameVoltCtr->AddFrame(new TGFrame(frameVoltCtr), new TGLayoutHints(kLHintsExpandX));
          
          TGTextButton* button4 = new TGTextButton(frameVoltCtr, "SAVE");
          button4->Connect("Clicked()", "MainFrame", this, "DoBiasVoltageSave()");
          frameVoltCtr->AddFrame(button4, hints);
          
          TGTextButton* button5 = new TGTextButton(frameVoltCtr, "LOAD");
          button5->Connect("Clicked()", "MainFrame", this, "DoBiasVoltageLoad()");
          frameVoltCtr->AddFrame(button5, hints);
          
          // spacer
          frameVoltCtr->AddFrame(new TGFrame(frameVoltCtr, 10), hints);
          
          TGTextButton* button6 = new TGTextButton(frameVoltCtr, "ZERO");
          button6->Connect("Clicked()", "MainFrame", this, "DoBoardSetAllPixelsZero()");
          frameVoltCtr->AddFrame(button6, hints);
          
          TGTextButton* button0 = new TGTextButton(frameVoltCtr, "SET");
          button0->Connect("Clicked()", "MainFrame", this, "DoBoardSetPixels()");
          frameVoltCtr->AddFrame(button0, hints);
        
        // all pixels bias voltage control
        TGHorizontalFrame* frameAllVolt = new TGHorizontalFrame(group);
        group->AddFrame(frameAllVolt, new TGLayoutHints(kLHintsExpandX));
          
          frameAllVolt->AddFrame(new TGLabel(frameAllVolt, "All pixels"), hintsLabel);
          frameAllVolt->AddFrame(new TGFrame(frameAllVolt), new TGLayoutHints(kLHintsExpandX)); // spacer
          
          entryAllPixels = new TGNumberEntryField(frameAllVolt, -1, 0, TGNumberFormat::kNESReal);
          entryAllPixels->SetDefaultSize(50, entryAllPixels->GetDefaultSize().fHeight);
          frameAllVolt->AddFrame(entryAllPixels, hints);
          
          buttonSetAll = new TGTextButton(frameAllVolt, "SET");
          buttonSetAll->Connect("Clicked()", "MainFrame", this, "DoBoardSetAllPixels()");
          frameAllVolt->AddFrame(buttonSetAll, hints);
        
        // voltage shift control
        TGHorizontalFrame* frameShift = new TGHorizontalFrame(group);
        group->AddFrame(frameShift, new TGLayoutHints(kLHintsExpandX));
          
          frameShift->AddFrame(new TGLabel(frameShift, "Voltage shift"), hintsLabel);
          frameShift->AddFrame(new TGFrame(frameShift), new TGLayoutHints(kLHintsExpandX)); // spacer
          
          entryVoltageShift = new TGNumberEntryField(frameShift, -1, 0, TGNumberFormat::kNESReal);
          entryVoltageShift->SetDefaultSize(50, entryVoltageShift->GetDefaultSize().fHeight);
          frameShift->AddFrame(entryVoltageShift, hints);
          
          TGTextButton* button1 = new TGTextButton(frameShift, "ADD");
          button1->Connect("Clicked()", "MainFrame", this, "DoBoardShiftPixels()");
          frameShift->AddFrame(button1, hints);
      
      // ADC Readout group
      TGGroupFrame* groupReadout = new TGGroupFrame(tabDACADC, "ADC Readout");
      //groupReadout->SetLayoutManager(new TGMatrixLayout(group3, 0, 2));
      tabDACADC->AddFrame(groupReadout, hints);
        
        // ADC readout indicators
        TGHorizontalFrame* frameReadout = new TGHorizontalFrame(groupReadout);
        groupReadout->AddFrame(frameReadout, hints);
          
          TGVerticalFrame* framePixelsReadout = new TGVerticalFrame(frameReadout);
          framePixelsReadout->SetLayoutManager(new TGMatrixLayout(framePixelsReadout, 0, 2));
          frameReadout->AddFrame(framePixelsReadout, hints);
          
          frameReadout->AddFrame(new TGVertical3DLine(frameReadout), new TGLayoutHints(kLHintsExpandY, 10, 10));
          //frameReadout->AddFrame(new TGHorizontal3DLine(frameReadout), hints);
          
          TGVerticalFrame* frameMonitoringReadout = new TGVerticalFrame(frameReadout);
          frameMonitoringReadout->SetLayoutManager(new TGMatrixLayout(frameMonitoringReadout, 0, 2));
          frameReadout->AddFrame(frameMonitoringReadout, hints);
            
            for (int i = 0; i < N_INPUT_CHANNELS; ++i) {
              TGVerticalFrame* frame = (i < N_PIXELS) ? framePixelsReadout : frameMonitoringReadout;
              
              TGLabel* label = new TGLabel(frame, InputChannels[i].name);
              frame->AddFrame(label, hints);
              
              entryADC[i] = new TGTextEntry(frame);
              entryADC[i]->SetAlignment(kTextRight);
              entryADC[i]->SetEnabled(kFALSE);
              frame->AddFrame(entryADC[i], hints);
            }
        
        // readout control
        TGHorizontalFrame* frameReadoutControl = new TGHorizontalFrame(groupReadout);
        groupReadout->AddFrame(frameReadoutControl, new TGLayoutHints(kLHintsExpandX));
          
          TGFrame* spacer = new TGFrame(frameReadoutControl);
          frameReadoutControl->AddFrame(spacer, new TGLayoutHints(kLHintsExpandX));
          
          TGTextButton* button3 = new TGTextButton(frameReadoutControl, "READ");
          button3->Connect("Clicked()", "MainFrame", this, "DoBoardRead()");
          frameReadoutControl->AddFrame(button3, hints);
      
      // Peltier group
      TGGroupFrame* groupPeltier = new TGGroupFrame(tabDACADC, "Peltier Cooler");
      tabDACADC->AddFrame(groupPeltier, hints);
        
        checkMBTAuto = new TGCheckButton(groupPeltier, "Autoregulation");
        groupPeltier->AddFrame(checkMBTAuto, hints);
        
        groupPeltier->AddFrame(new TGLabel(groupPeltier, "Temperature sensor"), hints);
        groupPeltier->AddFrame(new TGLabel(groupPeltier, InputChannels[PELTIER_TEMP_SENSOR].name), hints);
        
        groupPeltier->AddFrame(new TGLabel(groupPeltier, "Target temperature"), hints);
        entryMBT = new TGNumberEntryField(groupPeltier, -1, 18.0, TGNumberFormat::kNESRealOne);
        groupPeltier->AddFrame(entryMBT, hints);
        
        labelDeviation = new TGLabel(groupPeltier);
        labelDeviation->SetTextJustify(kTextLeft | kTextTop);
        groupPeltier->AddFrame(labelDeviation, new TGLayoutHints(kLHintsExpandX));
        
        // separator line
        groupPeltier->AddFrame(new TGHorizontal3DLine(groupPeltier), new TGLayoutHints(kLHintsExpandX, 0, 0, 5, 10));
        
        // direct control of Peltier voltage
        groupPeltier->AddFrame(new TGLabel(groupPeltier, "Peltier voltage"), hints);
        entryPCV = new TGNumberEntryField(groupPeltier, -1, 0, TGNumberFormat::kNESReal);
        entryPCV->Connect("ReturnPressed()", "MainFrame", this, "DoBoardSetPeltier()");
        groupPeltier->AddFrame(entryPCV, hints);
        
        TGTextButton* button2 = new TGTextButton(groupPeltier, "SET");
        button2->Connect("Clicked()", "MainFrame", this, "DoBoardSetPeltier()");
        groupPeltier->AddFrame(button2, hints);
  }
  
  // "Readout data" graphs tab
  {
    TGCompositeFrame* tabReadoutGraphs = tabs->AddTab("Readout data");
    tabReadoutGraphs->ChangeOptions((tabReadoutGraphs->GetOptions() & ~kVerticalFrame) | kHorizontalFrame);
      
      canvasADCReadout = new TRootEmbeddedCanvas(0, tabReadoutGraphs);
      tabReadoutGraphs->AddFrame(canvasADCReadout, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 2, 2, 2));
      
      TGVerticalFrame* frameControls = new TGVerticalFrame(tabReadoutGraphs);
      tabReadoutGraphs->AddFrame(frameControls, hints);
        
        // channels selection
        groupChannels = new TGButtonGroup(frameControls, "Display channels");
        groupChannels->SetLayoutManager(new TGMatrixLayout(groupChannels, 0, 2, 2, 0));
        groupChannels->Connect("Clicked(Int_t)", "MainFrame", this, "DoDisplayReadoutData()");
        frameControls->AddFrame(groupChannels, hints);
        
        for (int i = 0; i < 2*N_PIXELS; ++i) {
          const int ch = (i / 2) + ((i % 2 == 0) ? 0 : N_PIXELS);
          
          if (ch < N_INPUT_CHANNELS)
            //new TGCheckButton(groupChannels, TString::Format("%d %s", ch, InputChannels[ch].name), ch);
            new TGCheckButton(groupChannels, InputChannels[ch].name, ch);
          else
            groupChannels->AddFrame(new TGLabel(groupChannels));
        }
        
        // presets selection
        TGComboBox* comboPresets = new TGComboBox(frameControls);
        comboPresets->AddEntry("All", idAll);
        comboPresets->AddEntry("Pixels", idPixels);
        comboPresets->AddEntry("Pixels 1 - 8", idPixels1_8);
        comboPresets->AddEntry("Pixels 9 - 16", idPixels9_16);
        comboPresets->AddEntry("Pixels 17 - 24", idPixels16_24);
        comboPresets->AddEntry("Non pixels", idNonPixels);
        comboPresets->AddEntry("Peltier", idPeltier);
        comboPresets->AddEntry("Environment", idEnvironment);
        comboPresets->AddEntry("None", idNone);
        comboPresets->Resize(150, 20);
        // resize list to avoid vertical scrollbar
        TGListBox* list = comboPresets->GetListBox();
        const UInt_t h = list->GetNumberOfEntries() * list->GetItemVsize();
        list->Resize(list->GetWidth(), h);
        comboPresets->Connect("Selected(Int_t)", "MainFrame", this, "DoLoadPreset(Int_t)");
        comboPresets->Select(idAll, kTRUE);
        comboPresets->Connect("Selected(Int_t)", "MainFrame", this, "DoDisplayReadoutData()");
        frameControls->AddFrame(comboPresets, hints);
        
        // display type selection
        comboDisplayType = new TGComboBox(frameControls);
        comboDisplayType->AddEntry("Graph", idGraph);
        comboDisplayType->AddEntry("Histogram", idHistogram);
        comboDisplayType->Resize(150, 20);
        comboDisplayType->Select(idGraph);
        comboDisplayType->Connect("Selected(Int_t)", "MainFrame", this, "DoDisplayReadoutData()");
        frameControls->AddFrame(comboDisplayType, hints);
        
        // display legent
        checkShowLegend = new TGCheckButton(frameControls, "Show legend");
        checkShowLegend->Connect("Toggled(Bool_t)", "MainFrame", this, "DoDisplayReadoutData()");
        frameControls->AddFrame(checkShowLegend, hints);
        
        TGTextButton* buttonClear = new TGTextButton(frameControls, "Clear data");
        buttonClear->Connect("Clicked()", "MainFrame", this, "DoClearDAQData()");
        frameControls->AddFrame(buttonClear, hints);
  }
  
  // "DAQ control" tab
  {
    TGCompositeFrame* tabDAQ = tabs->AddTab("DAQ control");
    tabDAQhead = tabs->GetTabTab("DAQ control");
      
      // run DAQ control
      checkAuto = new TGCheckButton(tabDAQ, "Run DAQ");
      checkAuto->Connect("Toggled(Bool_t)", "MainFrame", this, "DoAutoRead(Bool_t)");
      tabDAQ->AddFrame(checkAuto, hints);
      
      // read loops control
      TGHorizontalFrame* frameRL = new TGHorizontalFrame(tabDAQ);
      tabDAQ->AddFrame(frameRL, hints);
        
        frameRL->AddFrame(new TGLabel(frameRL, "Read loops:"), hintsLabel);
        numberReadLoops = new TGNumberEntry(frameRL, 10, 5, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEAPositive);
        frameRL->AddFrame(numberReadLoops, hints);
      
      // DAQ interval control
      TGHorizontalFrame* frameDelay = new TGHorizontalFrame(tabDAQ);
      tabDAQ->AddFrame(frameDelay, hints);
        
        frameDelay->AddFrame(new TGLabel(frameDelay, "DAQ interval:"), hintsLabel);
        numberDelay = new TGNumberEntry(frameDelay, 1, 5, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEAPositive);
        numberDelay->Connect("TextChanged(char*)", "MainFrame", this, "DoSetTimerDelay()");
        frameDelay->AddFrame(numberDelay, hints);
        frameDelay->AddFrame(new TGLabel(frameDelay, "seconds"), hintsLabel);
      
      // DAQ buffer control
      TGGroupFrame* groupBuffer = new TGGroupFrame(tabDAQ, "Data Buffer");
      tabDAQ->AddFrame(groupBuffer, hints);
        
        // buffer length
        TGHorizontalFrame* frameHistory = new TGHorizontalFrame(groupBuffer);
        groupBuffer->AddFrame(frameHistory, hints);
        
          frameHistory->AddFrame(new TGLabel(frameHistory, "Buffer capacity:"), hintsLabel);
          numberHistoryLen = new TGNumberEntryField(frameHistory, -1, 100, TGNumberFormat::kNESInteger, TGNumberFormat::kNEAPositive);
          numberHistoryLen->SetDefaultSize(70, numberHistoryLen->GetDefaultSize().fHeight);
          frameHistory->AddFrame(numberHistoryLen, hints);
          frameHistory->AddFrame(new TGLabel(frameHistory, "entries"), hintsLabel);
        
        // buffer stats
        labelBufferSize = new TGLabel(groupBuffer);
        labelBufferSize->ChangeOptions((labelBufferSize->GetOptions() & ~kFitHeight) | kFixedHeight);
        labelBufferSize->SetTextJustify(kTextLeft | kTextCenterY);
        labelBufferSize->SetHeight(60);
        groupBuffer->AddFrame(labelBufferSize, new TGLayoutHints(kLHintsExpandX));
        
        // buffer save / clear
        TGHorizontalFrame* frameBufferCmd = new TGHorizontalFrame(groupBuffer);
        groupBuffer->AddFrame(frameBufferCmd, hints);
        
          TGTextButton* button1 = new TGTextButton(frameBufferCmd, "Save data");
          button1->Connect("Clicked()", "MainFrame", this, "DoSaveDAQData()");
          frameBufferCmd->AddFrame(button1, hints);
          
          frameBufferCmd->AddFrame(new TGFrame(frameBufferCmd, 20), hints);
          
          TGTextButton* button2 = new TGTextButton(frameBufferCmd, "Clear buffer");
          button2->Connect("Clicked()", "MainFrame", this, "DoClearDAQData()");
          frameBufferCmd->AddFrame(button2, hints);
  }
  
  // Pixels "Voltage scan" tab
  {
    TGCompositeFrame* tabIVRun = tabs->AddTab("Voltage scan");
    tabIvScanHead = tabs->GetTabTab("Voltage scan");
    tabIVRun->ChangeOptions((tabIVRun->GetOptions() & ~kVerticalFrame) | kHorizontalFrame);
      
      // plots area
      canvasIVRun = new TRootEmbeddedCanvas(0, tabIVRun);
      tabIVRun->AddFrame(canvasIVRun, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 2, 2, 2));
      
      // scan controls
      TGVerticalFrame* frame2 = new TGVerticalFrame(tabIVRun);
      tabIVRun->AddFrame(frame2, hints);
        
        // voltage ranges
        TGHorizontalFrame* frameRangesSL = new TGHorizontalFrame(frame2);
        frame2->AddFrame(frameRangesSL, new TGLayoutHints(kLHintsExpandX));
          
          frameRangesSL->AddFrame(new TGLabel(frameRangesSL, "Voltage ranges:"), hintsLabel);
          frameRangesSL->AddFrame(new TGFrame(frameRangesSL), new TGLayoutHints(kLHintsExpandX)); // spacer
          
          TGTextButton* button1 = new TGTextButton(frameRangesSL, "SAVE");
          button1->Connect("Clicked()", "MainFrame", this, "DoIVRangesSave()");
          frameRangesSL->AddFrame(button1, hints);
          
          TGTextButton* button2 = new TGTextButton(frameRangesSL, "LOAD");
          button2->Connect("Clicked()", "MainFrame", this, "DoIVRangesLoad()");
          frameRangesSL->AddFrame(button2, hints);
        
        // voltage scan ranges
        TGVerticalFrame* frameRanges = new TGVerticalFrame(frame2);
        frameRanges->SetLayoutManager(new TGMatrixLayout(frameRanges, 0, 3));
        frame2->AddFrame(frameRanges, hints);
          
          // header line
          frameRanges->AddFrame(new TGLabel(frameRanges, "start"), hints);
          frameRanges->AddFrame(new TGLabel(frameRanges, "stop"), hints);
          frameRanges->AddFrame(new TGLabel(frameRanges, "step"), hints);
          
          // add 2 lines
          const double a[IVRanges] = {0, 68, 4, 68, 73, 0.1};
          for (size_t i = 0; i < IVRanges; ++i) {
            TGNumberEntryField* entry = new TGNumberEntryField(frameRanges, -1, a[i], TGNumberFormat::kNESReal, TGNumberFormat::kNEANonNegative);
            entry->SetDefaultSize(60, entry->GetDefaultSize().fHeight);
            entry->Resize(); // necessary to fix layout with TGMatrixLayout
            frameRanges->AddFrame(entry, hints);
            entryRange[i] = entry;
          }
          
          // add <TAB> navigation between voltage ranges fields
          for (size_t i = 0; i < IVRanges; ++i) {
            const size_t nexti = (i + 1) % IVRanges;
            entryRange[i]->Connect("TabPressed()", "TGNumberEntryField", entryRange[nexti], "SetFocus()");
          }
        
        // delay interval for voltage settle
        TGHorizontalFrame* frameDelay = new TGHorizontalFrame(frame2);
        frame2->AddFrame(frameDelay, hints);
          
          frameDelay->AddFrame(new TGLabel(frameDelay, "Delay:"), hintsLabel);
          step_delay = new TGNumberEntry(frameDelay, 0.1, 5, -1, TGNumberFormat::kNESReal, TGNumberFormat::kNEANonNegative);
          frameDelay->AddFrame(step_delay, hints);
          frameDelay->AddFrame(new TGLabel(frameDelay, "seconds"), hintsLabel);
        
        // voltage scan: run / save results buttons
        TGHorizontalFrame* frame3 = new TGHorizontalFrame(frame2);
        frame2->AddFrame(frame3, hints);
          
          TGTextButton* button4 = new TGTextButton(frame3, "Run scan");
          button4->Connect("Clicked()", "MainFrame", this, "DoIVRun()");
          frame3->AddFrame(button4, hints);
          
          // save scan results
          TGTextButton* button6 = new TGTextButton(frame3, "Save data");
          button6->Connect("Clicked()", "MainFrame", this, "DoSaveIVData()");
          frame3->AddFrame(button6, hints);
        
        // separator line
        frame2->AddFrame(new TGHorizontal3DLine(frame2), new TGLayoutHints(kLHintsExpandX, 0, 0, 10, 10));
        
        // display options: channels filter
        comboChGroup = new TGComboBox(frame2);
        comboChGroup->AddEntry("All channels", idChAll);
        comboChGroup->AddEntry("Channels 1 - 8", idCh1_8);
        comboChGroup->AddEntry("Channels 9 - 16", idCh9_16);
        comboChGroup->AddEntry("Channels 17 - 24", idCh17_24);
        comboChGroup->Resize(150, 20);
        comboChGroup->Select(idChAll, kFALSE);
        comboChGroup->Connect("Selected(Int_t)", "MainFrame", this, "DoDisplayIVData()");
        frame2->AddFrame(comboChGroup, hints);
        
        // display options: graph type
        comboDisplay = new TGComboBox(frame2);
        comboDisplay->AddEntry("I vs. V", idIV);
        comboDisplay->AddEntry("(dI/dV / I) vs. V", iddIdV);
        comboDisplay->Resize(150, 20);
        comboDisplay->Select(idIV, kFALSE);
        comboDisplay->Connect("Selected(Int_t)", "MainFrame", this, "DoDisplayIVData()");
        frame2->AddFrame(comboDisplay, hints);
        
        // display options: legend
        checkVSLegend = new TGCheckButton(frame2, "Show legend");
        checkVSLegend->Connect("Toggled(Bool_t)", "MainFrame", this, "DoDisplayIVData()");
        frame2->AddFrame(checkVSLegend, hints);
        
        // separator line
        frame2->AddFrame(new TGHorizontal3DLine(frame2), new TGLayoutHints(kLHintsExpandX, 0, 0, 10, 10));
        
        // breakdown voltage list
        labelVbreakdown = new TGLabel(frame2);
        labelVbreakdown->SetTextJustify(kTextLeft | kTextTop);
        frame2->AddFrame(labelVbreakdown, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
        
        // control to set pixels bias voltage
        TGTextButton* button5 = new TGTextButton(frame2, "Set pixels to breakdown voltage");
        button5->Connect("Clicked()", "MainFrame", this, "DoSetPixelsBreakdown()");
        frame2->AddFrame(button5, hints);
  }
  
  // setup timer period and action (for DAQ)
  DoSetTimerDelay();
  timer.Connect("Timeout()", "MainFrame", this, "DoBoardRead()");
  
  // arrange widgets
  MapSubwindows();
  MapWindow();
  Layout();
  
  setStatus("Opening SiPM Board ...");
  board = new SiPM_Board;
  
  const string log = board->cleanLog();
  labelHardware->SetText(log.c_str());
  
  if (*board) {
    // load profile and reset bias voltage
    comboProfile->Select(0, kTRUE);
    
    // activate timer (run DAQ)
    checkAuto->SetOn(kTRUE, kTRUE);
    
    setStatus("OK");
  }
  else {
    setStatus("SiPM Board is not found");
  }
}

MainFrame::~MainFrame()
{
  Cleanup();
  delete board;
}


void MainFrame::DoLoadProfile(Int_t id)
{
  const char* fname = ((TGTextLBEntry*) comboProfile->GetListBox()->GetEntry(id))->GetTitle();
  SetWindowName(TString::Format("%s : %s", gROOT->GetName(), fname));
  
  const Profile* p = new Profile(fname);
  // TODO: could be crash if DAQ loop is active and use profile
  delete board->profile;
  board->profile = p;
  
  // display profile info
  ostringstream oss;
  oss << *p;
  
  labelProfile->SetText(oss.str().c_str());
  
  // reset voltage
  entryAllPixels->SetNumber(0);
  DoBoardSetAllPixels();
  
  // reset history
  DoClearDAQData();
}

void  MainFrame::setDacStatus(const DACStatus status)
{
  Pixel_t color = entryDAC[0]->GetWhitePixel();
  if (status == idSuccess) color = 0xd1e6d6; // green
  if (status == idFail) color = 0xe6d3d1; // red
  
  // TODO: use yellow color for channels with big voltage error
  
  for (int i = 0; i < N_PIXELS; ++i) {
    entryDAC[i]->ChangeBackground(color);
    
    // TODO: the tooltip should be reset if user starts edit of DAC fields
    //       but simple implementation do not work as expected
    //entryDAC[i]->SetToolTipText("");
  }
}

void MainFrame::DoEntryDacChanged()
{
  setDacStatus(idChanged);
}

// PID controller to drive Peltier element
// http://en.wikipedia.org/wiki/PID_controller
// input: process variable, setpoint, time step
double PELT_PID_Controller(const double PV, const double SP, const double dt)
{
  // gains:
  const double Kp = 0.5; // proportional
  const double Ki = 0.1; // integral
  const double Kd = 0.05; // derivative
  
  const double error = PV - SP;
  
  // integral term
  static double integral = 0.;
  integral += error * dt;
  
  // derivative term
  static double prev_error = error;
  const double derivative = (error - prev_error) / dt;
  prev_error = error;
  
  // output
  const double output = Kp*error + Ki*integral + Kd*derivative;
  
  // debug output
  /*
  cout << "output = "
       << Kp << " * " << error << " + "
       << Ki << " * " << integral << " + "
       << Kd << " * " << derivative
       << " = " << output << endl;
  */
  
  return output;
}

void MainFrame::ControlPeltier()
{
  // do temperature autoregulation
  labelDeviation->SetText("");
  
  if (checkMBTAuto->IsOn()) {
    const size_t N = history.size();
    
    if (N < 2) return;
    
    const double temp_pv = history[N - 1].adc[PELTIER_TEMP_SENSOR].value; // current temperature
    const double temp_sp = entryMBT->GetNumber();                         // target temperature
    const double dt = history[N - 1].time - history[N - 2].time;          // time step
    
    const double pelt_v = PELT_PID_Controller(temp_pv, temp_sp, dt);
    
    entryPCV->SetNumber(pelt_v);
    DoBoardSetPeltier();
    
    // calc average deviation over readouts history
    double S = 0;
    for (size_t i = 1; i < N; ++i) {
      const double dti = history[i].time - history[i - 1].time;
      S += fabs(history[i].adc[PELTIER_TEMP_SENSOR].value - temp_sp) * dti;
    }
    
    const double total_dt = history[N - 1].time - history[0].time;
    const double avg = (total_dt > 0) ? S / total_dt : 0.;
    
    labelDeviation->SetText(TString::Format("Deviation: %.3f C", avg));
  }
}

void MainFrame::DoBoardRead()
{
  // disable timer to prevent possible double-call of DoBoardRead()
  // TODO: this is not perfect way, user can reactivate timer by button "READ" on the control tab
  //       or checkbox "run DAQ" on DAQ tab. Use mutex?
  timer.Stop();
  
  // read ADC channels
  const int nloops = numberReadLoops->GetIntNumber();
  const InData& r = board->read(nloops);
  
  // cleanup communication log to avoid memory leak
  board->cleanLog();
  
  // readout failed (probably the board is not connected), exit
  if (r.adc.empty()) return;
  
  // remove old entries from read buffer
  const int bufcap = numberHistoryLen->GetIntNumber();
  while (history.size() >= bufcap) history.pop_front();
  
  // and from write buffer (minimum buffer size is 1 entry)
  const int obufcap = (bufcap > 0) ? bufcap : 1;
  while (ohistory.size() >= obufcap) ohistory.pop_front();
  
  // save last readout
  history.push_back(r);
  
  ControlPeltier();
  
  // update interface
  for (size_t i = 0; i < r.adc.size(); ++i)
    entryADC[i]->SetText(TString::Format("%.3f %s", r.adc[i].value, r.adc[i].channel->unit));
  
  // update data stats
  const size_t N = history.size();
  const double duration = (N > 0) ? (history[N-1].time - history[0].time) : 0.;
  labelBufferSize->SetText(TString::Format("Output buffer: %d entries\nInput buffer: %d entries\nDuration: %.0f seconds", ohistory.size(), N, duration));
  
  // update ADC graphs
  DoDisplayReadoutData();
  
  // restore timer
  const Bool_t autoREAD = checkAuto->IsOn();
  if (autoREAD) timer.Start();
}

void MainFrame::DoBoardSetPixels()
{
  vector <double> bv(N_PIXELS);
  
  for (size_t i = 0; i < N_PIXELS; ++i) {
    bv[i] = entryDAC[i]->GetNumber();
  }
  
  const OutData& od = board->setPixelsBV(bv);
  ohistory.push_back(od);
  
  for (size_t i = 0; i < N_PIXELS; ++i) {
    const double quant = board->profile->calibration[i].slope;
    const double error = od.bv[i] - bv[i];
    if (fabs(error) > quant)
      entryDAC[i]->SetNumber(od.bv[i]);
    
    entryDAC[i]->SetToolTipText(TString::Format("DAC channel: %d\nVoltage quant: %f V\nRequested voltage: %f V\nDAC voltage: %f V\nVoltage error: %+f V\nDAC data: %d (hex: %03X)", i, quant, bv[i], od.bv[i], error, od.bv_data[i], od.bv_data[i]));
  }
  
  setDacStatus(od.ok ? idSuccess : idFail);
}

void MainFrame::DoBoardSetAllPixels()
{
  // set all Pixels BV
  const double voltage = entryAllPixels->GetNumber();
  
  for (size_t i = 0; i < N_PIXELS; ++i)
    entryDAC[i]->SetNumber(voltage);
  
  DoBoardSetPixels();
}

void MainFrame::DoBoardSetAllPixelsZero()
{
  // set all Pixels BV to 0
  const double voltage = 0;
  
  for (size_t i = 0; i < N_PIXELS; ++i)
    entryDAC[i]->SetNumber(voltage);
  
  DoBoardSetPixels();
}

void MainFrame::DoBiasVoltageSave()
{
  // select file
  const char* fileTypes[] = {"Voltage List File", "*.voltage", 0};
  TGFileInfo fFileInfo;
  fFileInfo.fFileTypes = fileTypes;
  new TGFileDialog(gClient->GetRoot(), this, kFDSave, &fFileInfo);
  // file is not selected
  if (!fFileInfo.fFilename) return;
  
  ofstream f(fFileInfo.fFilename);
  for (size_t i = 0; i < N_PIXELS; ++i) {
    f << entryDAC[i]->GetNumber() << "\n";
  }
}

void MainFrame::DoBiasVoltageLoad()
{
  // select file
  const char* fileTypes[] = {"Voltage List File", "*.voltage", 0};
  TGFileInfo fFileInfo;
  fFileInfo.fFileTypes = fileTypes;
  new TGFileDialog(gClient->GetRoot(), this, kFDOpen, &fFileInfo);
  // file is not selected
  if (!fFileInfo.fFilename) return;
  
  ifstream f(fFileInfo.fFilename);
  
  double voltage;
  size_t i = 0;
  
  while (f >> voltage) {
    entryDAC[i++]->SetNumber(voltage);
    if (i >= N_PIXELS) break;
  }
  
  DoBoardSetPixels();
}

void MainFrame::DoBoardShiftPixels()
{
  const double shift = entryVoltageShift->GetNumber();
  
  for (size_t i = 0; i < N_PIXELS; ++i) {
    const double v = entryDAC[i]->GetNumber();
    entryDAC[i]->SetNumber(v + shift);
  }
  
  DoBoardSetPixels();
}

void MainFrame::DoBoardSetPeltier()
{
  // set Peltier Element voltage
  const double voltage = entryPCV->GetNumber();
  const OutData& od = board->setPeltierV(voltage);
  ohistory.push_back(od);
  
  const double quant = PELTIER_RANGE / 4096.;
  const double error = od.pv - voltage;
  if (fabs(error) > quant)
    entryPCV->SetNumber(od.pv);
  
  entryPCV->SetToolTipText(TString::Format("DAC channel: %d\nVoltage quant: %f V\nRequested voltage: %f V\nDAC voltage: %f V\nVoltage error: %+f V\nDAC data: %d (hex: %03X)", DAC_CH_PELTIER, quant, voltage, od.pv, error, od.pv_data, od.pv_data));
}

void MainFrame::DoLoadPreset(const Int_t id)
{
  for (int i = 0; i < N_INPUT_CHANNELS; ++i) {
    const bool mask =
      ((id == idAll)         && true) ||
      ((id == idPixels)      && (i < N_PIXELS)) ||
      ((id == idPixels1_8)   && (i < 8)) ||
      ((id == idPixels9_16)  && (8 <= i && i < 16)) ||
      ((id == idPixels16_24) && (16 <= i && i < 24)) ||
      ((id == idNonPixels)   && (i >= N_PIXELS)) ||
      ((id == idPeltier)     && (i == 27 || i == 28)) ||
      ((id == idEnvironment) && (i == 29 || i == 30 || i == 31 || i == 33)) ||
      ((id == idNone)        && false);
    
    groupChannels->Find(i)->SetOn(mask ? kTRUE : kFALSE);
  }
}

void MainFrame::DoSetTimerDelay()
{
  const int d = numberDelay->GetIntNumber(); // in seconds
  if (d > 0) timer.SetTime(d*1000);
  
  //new TGMsgBox(gClient->GetRoot(), this, "Debug", TString::Format("Delay: %d seconds", d));
}

void MainFrame::DoSaveDAQData()
{
  // select file
  const char* fileTypes[] = {"ROOT File", "*.root", 0};
  TGFileInfo fFileInfo;
  fFileInfo.fFileTypes = fileTypes;
  new TGFileDialog(gClient->GetRoot(), this, kFDSave, &fFileInfo);
  // file is not selected
  if (!fFileInfo.fFilename) return;
  
  // append .root suffix
  TString fname(fFileInfo.fFilename);
  if (!fname.EndsWith(".root")) fname += ".root";
  
  TFile f(fname, "recreate");
  
  // save input data
  TTree t("in_channels", "in_channels");
  
  // book tree
  Double_t time = 0;
  t.Branch("time", &time, "time/D");
  
  Float_t var[N_INPUT_CHANNELS] = {0};
  for (size_t j = 0; j < N_INPUT_CHANNELS; ++j) {
    TString name = history[0].adc[j].channel->name;
    name.ReplaceAll(" ", "_");
    name.ReplaceAll("+", "");
    name.ReplaceAll("*", "");
    name.ReplaceAll(".", "_");
    t.Branch(name, &var[j], name + "/F");
  }
  
  // fill tree
  for (size_t i = 0; i < history.size(); ++i) {
    time = history[i].time;
    
    for (size_t j = 0; j < N_INPUT_CHANNELS; ++j)
      var[j] = history[i].adc[j].value;
    
    t.Fill();
  }
  
  t.Write();
  
  // save output data
  TTree ot("out_channels", "out_channels");
  
  // book tree
  time = 0;
  ot.Branch("time", &time, "time/D");
  
  for (size_t j = 0; j < N_PIXELS; ++j) {
    var[j] = 0;
    
    TString name = history[0].adc[j].channel->name;
    name.ReplaceAll("I_", "BV_");
    name.ReplaceAll(" ", "_");
    name.ReplaceAll("+", "");
    name.ReplaceAll("*", "");
    name.ReplaceAll(".", "_");
    ot.Branch(name, &var[j], name + "/F");
  }
  
  Float_t pv = 0;
  ot.Branch("PV", &pv, "PV/F");
  
  // fill tree
  for (size_t i = 0; i < ohistory.size(); ++i) {
    time = ohistory[i].time;
    pv = ohistory[i].pv;
    
    for (size_t j = 0; j < N_PIXELS; ++j)
      var[j] = ohistory[i].bv[j];
    
    ot.Fill();
  }
  
  ot.Write();
  
}

void MainFrame::DoClearDAQData()
{
  // clear read buffer
  history.clear();
  
  // crear write buffer (keep only last entry)
  while (ohistory.size() > 1) ohistory.pop_front();
}


void cleanCanvas(TCanvas* c)
{
  TList* list = c->GetListOfPrimitives();
  list->SetOwner(kTRUE);
  list->Delete();
  //TIter next(list);
  //TObject* obj;
  //cout << obj->GetName() << " " << obj->GetTitle() << endl;
  //while (obj = next()) delete obj;
  
  c->Clear();
}

TLegend* buildLegend(TMultiGraph* mg)
{
  TLegend* legend = new TLegend(0.12, 0.20, 0.19, 0.88);
  
  TIter next(mg->GetListOfGraphs());
  while (TGraph* gr = (TGraph*) next())
    legend->AddEntry(gr, "", "LP");
  
  legend->SetFillColor(0);
  //legend->SetBorderSize(0);
  //legend->SetTextFont(43);
  //legend->SetTextSize(20);
  
  // update legend size according to number of entries
  const double y1 = legend->GetY1();
  const double y2 = legend->GetY2();
  const double y1_new = y2 - (y2 - y1) * legend->GetNRows() / N_INPUT_CHANNELS;
  legend->SetY1(y1_new);
  
  return legend;
}


// TODO: redo as class with getGraphIV/getGraphdIdVI/getVbreakdown + memory management
//  TMultiGraph* getIVgraph() const;
//  TMultiGraph* getdIdVgraph() const;
struct IVResults {
  TGraph* IV[N_PIXELS];
  TGraph* dIdVI[N_PIXELS];
  
  double Vb[N_PIXELS];
  bool Vb_detected[N_PIXELS];
};

// process voltage scan data
IVResults ProcessIVData(const vector<DataPoint>& ivdata)
{
  IVResults r;
  
  const size_t n = ivdata.size();
  double* x = new double[n];
  double* y = new double[n];
  
  for (size_t i = 0; i < N_PIXELS; ++i) {
    double prev_v = 0;
    size_t m = 0;
    
    // channel name
    const char* name = (n > 0) ? ivdata[0].in.adc[i].channel->name : "";
    
    // calc I vs V plot
    for (size_t j = 0; j < n; ++j) {
      x[m] = ivdata[j].out.bv[i];
      y[m] = ivdata[j].in.adc[i].value;
      
      // skip point if the voltage is the same as in previous point
      // TODO: do average over same-voltage points
      if ((j > 0) && (x[m] == prev_v)) continue;
      
      prev_v = x[m];
      m++;
    }
    
    // at this line (x[], y[], m) is V,I curve for channel i
    r.IV[i] = new TGraph(m, x, y);
    r.IV[i]->SetNameTitle(name, name);
    
    // calc dI/dV/I vs V plot
    m = (m > 1) ? m - 1 : 0;
    for (size_t j = 0; j < m; ++j) {
      const double v1 = x[j];
      const double v2 = x[j + 1];
      const double i1 = y[j];
      const double i2 = y[j + 1];
      const double i_mean = 0.5*(i1 + i2);
      const double v_mean = 0.5*(v1 + v2);
      const double dI = i2 - i1;
      const double dV = v2 - v1;
      
      x[j] = v_mean;
      y[j] = dI / dV / i_mean;
      
      // check/fix for Inf or NaN value
      if (_finite(y[j]) == 0) y[j] = 0;
    }
    
    r.dIdVI[i] = new TGraph(m, x, y);
    r.dIdVI[i]->SetNameTitle(name, name);
    
    // calc breakdown voltage - peak search on 'dI/dV/I vs V' plot
    Double_t* source = new Double_t[m];
    Double_t* dest   = new Double_t[m];
    for (size_t j = 0; j < m; ++j) source[j] = y[j];
    TSpectrum s(10);
    const Int_t npeaks = s.SearchHighRes(source, dest, m, 3, 10, kFALSE, 3, kFALSE, 3);
    delete [] source;
    delete [] dest;
    
    const float pos = s.GetPositionX()[0];
    const size_t idx = pos;
    const bool Vb_detected = (npeaks > 0) && (idx >=0) && (idx + 1 < m);
    const double Vb = Vb_detected ? x[idx] + (x[idx+1] - x[idx])*(pos - idx) : 0;
    
    //cout << "ch = " << (i + 1) << " npeaks = " << npeaks << " Vb_detected = " << Vb_detected << " pos = " << pos << " Vb = " << Vb << endl;
    //for (size_t j = 0; j < npeaks; ++j)
    //  cout << "          x = " << s.GetPositionX()[j] << endl;
    
    r.Vb_detected[i] = Vb_detected;
    r.Vb[i] = Vb;
  }
  
  delete [] x;
  delete [] y;
  
  return r;
}

vector<bool> MainFrame::getIVselection() const
{
  vector<bool> selection(N_PIXELS);
  
  const Int_t id = comboChGroup->GetSelected();
  
  for (size_t i = 0; i < selection.size(); ++i) {
    selection[i] =
      ((id == idChAll)   && true) ||
      ((id == idCh1_8)   && (i < 8)) ||
      ((id == idCh9_16)  && (8 <= i && i < 16)) ||
      ((id == idCh17_24) && (16 <= i && i < 24));
  }
  
  return selection;
}

TMultiGraph* buildMultiGraph(TGraph* graphs[N_PIXELS], const char* name, const vector<bool>& mask = vector<bool>(N_PIXELS, true))
{
  // prepare plot
  TMultiGraph* mg = new TMultiGraph(name, name);
  int color = 1;
  
  for (size_t i = 0; i < mask.size(); ++i) {
    // skip white color
    if (color == 10) color++;
    
    // skip unselected channels
    if (!mask[i]) {
      delete graphs[i];
      continue;
    }
    
    TGraph* gr = graphs[i];
    gr->SetMarkerColor(color);
    gr->SetMarkerStyle(kFullDotMedium);
    gr->SetMarkerSize(1.0);
    gr->SetLineColor(color);
    gr->SetLineStyle(1);
    gr->SetLineWidth(1);
    
    mg->Add(gr);
    
    color++;
  }
  
  return mg;
}

std::vector<double> MainFrame::listIvScanVoltages()
{
  // prepare list of voltage points
  vector<double> list;
  
  // process ranges
  for (size_t i = 0; i < IVRanges/3; ++i) {
    const double start = entryRange[3*i + 0]->GetNumber();
    const double stop  = entryRange[3*i + 1]->GetNumber();
    const double step  = entryRange[3*i + 2]->GetNumber();
    
    if (step > 0)
      for (double x = start; x < stop; x += step)
        list.push_back(x);
  }
  
  // sort and remove duplicates
  sort(list.begin(), list.end());
  vector<double>::iterator last = unique(list.begin(), list.end());
  list.erase(last, list.end());
  
  return list;
}

void MainFrame::DoIVRun()
{
  // disable timer (stop DAQ) to avoid interference of automatic readout with IV test
  const Bool_t autoREAD = checkAuto->IsOn();
  checkAuto->SetOn(kFALSE, kTRUE);
  checkAuto->SetEnabled(kFALSE);
  // TODO: also disable "READ" button on control tab
  
  tabIvScanHead->ChangeBackground(tabIvScanHead->GetDefaultSelectedBackground());
  setStatus("Starting voltage scan ...");
  
  const vector<double>& list = listIvScanVoltages();
  const int nloops = numberReadLoops->GetIntNumber();
  
  ivdata.clear();
  
  // do scan
  for (size_t i = 0 ; i < list.size(); ++i) {
    setStatus(TString::Format("Voltage scan: %.3f V (step %d of %d)", list[i], (i + 1), list.size()));
    
    // set bias voltage
    const OutData& od = board->setPixelsBV(list[i]);
    ohistory.push_back(od);
    
    // wait
    gSystem->Sleep(1000*step_delay->GetNumber());
    
    // read currents
    const InData& id = board->read(nloops);
    history.push_back(id);
    
    const DataPoint p = {od, id};
    ivdata.push_back(p);
    
    // update plots
    DoDisplayIVData();
    
    // adjust peltier
    ControlPeltier();
  }
  
  // reset bias voltage to zero
  entryAllPixels->SetNumber(0);
  buttonSetAll->Clicked();
  
  setStatus("Voltage scan completed");
  tabIvScanHead->ChangeBackground(tabIvScanHead->GetDefaultFrameBackground());
  
  // restore timer
  checkAuto->SetEnabled(kTRUE);
  checkAuto->SetOn(autoREAD, kTRUE);
}

void MainFrame::DoDisplayIVData()
{
  TCanvas* c = canvasIVRun->GetCanvas();
  cleanCanvas(c);
  
  IVResults r = ProcessIVData(ivdata);
  
  // display plots
  const Int_t id = comboDisplay->GetSelected();
  const vector<bool>& mask = getIVselection();
  TMultiGraph* mg = 0;
  
  if (id == idIV) {
    mg = buildMultiGraph(r.IV, "i_vs_v", mask);
    for (size_t i = 0; i < N_PIXELS; ++i)
      delete r.dIdVI[i];
    
  } else if (id == iddIdV) {
    mg = buildMultiGraph(r.dIdVI, "di_idv_vs_v", mask);
    for (size_t i = 0; i < N_PIXELS; ++i)
      delete r.IV[i];
    
  } else {
    for (size_t i = 0; i < N_PIXELS; ++i) {
      delete r.IV[i];
      delete r.dIdVI[i];
    }
    
    return;
  }
  
  mg->Draw("ALP");
  if (checkVSLegend->IsOn()) buildLegend(mg)->Draw();
  
  c->Update();
  
  // display breakdown voltage values
  ostringstream oss;
  oss << "Breakdown voltage:";
  
  for (size_t i = 0; i < N_PIXELS; ++i) {
    if (!mask[i]) continue;
    
    oss << "\n"
        << "  ch" << (i + 1) << " = ";
    if (r.Vb_detected[i]) oss << r.Vb[i];
    else oss << "n\\a";
  }
  
  labelVbreakdown->SetText(oss.str().c_str());
  
  // force label resize to fit to new text
  ((TGFrame*)labelVbreakdown->GetParent()->GetParent())->Layout();
}

void MainFrame::DoSetPixelsBreakdown()
{
  IVResults r = ProcessIVData(ivdata);
  
  for (size_t i = 0; i < N_PIXELS; ++i) {
    entryDAC[i]->SetNumber(r.Vb[i]);
  }
  
  // TODO: memory leak IVResults r
}

void MainFrame::DoSaveIVData()
{
  // select file
  const char* fileTypes[] = {"ROOT File", "*.root", 0};
  TGFileInfo fFileInfo;
  fFileInfo.fFileTypes = fileTypes;
  new TGFileDialog(gClient->GetRoot(), this, kFDSave, &fFileInfo);
  // file is not selected
  if (!fFileInfo.fFilename) return;
  
  TFile f(fFileInfo.fFilename, "recreate");
  
  // create graphs
  IVResults r = ProcessIVData(ivdata);
  TMultiGraph* mgs[] = {buildMultiGraph(r.IV, "i_vs_v"), buildMultiGraph(r.dIdVI, "di_idv_vs_v")};
  
  // save graphs
  for (int i = 0; i < 2; ++i) {
    TMultiGraph* mg = mgs[i];
    const char* name = mg->GetName();
    
    // all graphs in one object "name-all"
    f.cd();
    mg->Write(TString::Format("%s-all", name));
    
    // each graph separately in subdirectory "name/"
    f.mkdir(name)->cd();
    TIter next(mg->GetListOfGraphs());
    while (TObject* obj = next())
      obj->Write();
  }
  
  // TODO: memory leak
  //for (size_t i = 0; i < N_PIXELS; ++i) {
  //  delete r.IV[i];
  //  delete r.dIdVI[i];
  // }
}

void MainFrame::DoIVRangesSave()
{
  // select file
  const char* fileTypes[] = {"Voltage Ranges List", "*.range", 0};
  TGFileInfo fFileInfo;
  fFileInfo.fFileTypes = fileTypes;
  new TGFileDialog(gClient->GetRoot(), this, kFDSave, &fFileInfo);
  // file is not selected
  if (!fFileInfo.fFilename) return;
  
  ofstream f(fFileInfo.fFilename);
  for (size_t i = 0; i < IVRanges/3; ++i) {
    const double start = entryRange[3*i + 0]->GetNumber();
    const double stop  = entryRange[3*i + 1]->GetNumber();
    const double step  = entryRange[3*i + 2]->GetNumber();
    f << start << "\t" << stop << "\t" << step << "\n";
  }
}

void MainFrame::DoIVRangesLoad()
{
  // select file
  const char* fileTypes[] = {"Voltage Ranges List", "*.range", 0};
  TGFileInfo fFileInfo;
  fFileInfo.fFileTypes = fileTypes;
  new TGFileDialog(gClient->GetRoot(), this, kFDOpen, &fFileInfo);
  // file is not selected
  if (!fFileInfo.fFilename) return;
  
  ifstream f(fFileInfo.fFilename);
  
  double x;
  size_t i = 0;
  
  while (f >> x) {
    entryRange[i++]->SetNumber(x);
    if (i >= IVRanges) break;
  }
}


void MainFrame::DoMenu(Int_t id)
{
  switch (id) {
    case idFileExit: CloseWindow(); break;
    case idHelpAbout: HelpAbout(); break;
  }
}

void MainFrame::DoAutoRead(Bool_t on)
{
  if (on == kTRUE) {
    timer.Start();
    tabDAQhead->ChangeBackground(tabDAQhead->GetDefaultSelectedBackground());
  }
  else {
    timer.Stop();
    tabDAQhead->ChangeBackground(tabDAQhead->GetDefaultFrameBackground());
  }
}

void MainFrame::UpdateADCGraphs()
{
  const int N = history.size();
  if (N < 1) return;
  
  double* t = new double[N];
  double* y = new double[N];
  const double tN = history[N - 1].time;
  
  TLegend* legend = new TLegend(0.12, 0.20, 0.27, 0.88);
  TMultiGraph* mg = new TMultiGraph;
  int color = 1;
  
  for (size_t i = 0; i < N_INPUT_CHANNELS; ++i) {
    if (color == 10) color++; // skip white color
    
    if (! groupChannels->Find(i)->IsOn()) continue;
    
    for (size_t j = 0; j < N; ++j) {
      t[j] = (history[j].time - tN);
      y[j] = history[j].adc[i].value;
    }
    
    TGraph* gr = new TGraph(N, t, y);
    gr->SetTitle(history[N - 1].adc[i].channel->name);
    
    gr->SetMarkerColor(color);
    gr->SetMarkerStyle(kFullDotMedium);
    gr->SetMarkerSize(1.0);
    
    gr->SetLineColor(color);
    gr->SetLineStyle(1);
    gr->SetLineWidth(1);
    
    legend->AddEntry(gr, "", "LP");
    mg->Add(gr);
    
    color++;
  }
  
  delete [] t;
  delete [] y;
  
  legend->SetFillColor(0);
  //legend->SetBorderSize(0);
  //legend->SetTextFont(43);
  //legend->SetTextSize(20);
  
  // update legend size according to number of entries
  const double y1 = legend->GetY1();
  const double y2 = legend->GetY2();
  const double y1_new = y2 - (y2 - y1) * legend->GetNRows() / N_INPUT_CHANNELS;
  legend->SetY1(y1_new);
  
  TCanvas* c = canvasADCReadout->GetCanvas();
  //c->Clear();
  cleanCanvas(c);
  
  // no data to draw, exit
  //if (! mg->GetListOfGraphs()) return;
  
  mg->Draw("ALP");
  //mg->GetXaxis()->SetTitle("Time, s");
  //mg->GetYaxis()->SetTitle("Pixels Current");
  
  /*
  mg->GetXaxis()->SetNdivisions(20505);
  mg->GetXaxis()->SetLabelSize(0.035);
  mg->GetXaxis()->SetLabelFont(42);
  mg->GetXaxis()->SetTitleFont(42);
  
  mg->GetYaxis()->SetLabelSize(0.035);
  mg->GetYaxis()->SetLabelOffset(0.015);
  mg->GetYaxis()->SetLabelFont(42);
  mg->GetYaxis()->SetTitleFont(42);
  mg->GetYaxis()->SetTitleOffset(1.35);
  */
  //mg->GetYaxis()->SetRangeUser(minYAxis , maxYAxis);
  
  if (checkShowLegend->IsOn() && legend->GetNRows() > 0) legend->Draw();
  c->Update();
}

void MainFrame::UpdateADCHistograms()
{
  const int N = history.size();
  if (N < 1) return;
  
  TLegend* legend = new TLegend(0.12, 0.20, 0.27, 0.88);
  THStack* hs = new THStack;
  int color = 1;
  
  // find min and max
  double min = 1e9;
  double max = -1e9;
  
  for (size_t i = 0; i < N_INPUT_CHANNELS; ++i) {
    if (! groupChannels->Find(i)->IsOn()) continue;
    
    for (size_t j = 0; j < N; ++j) {
      const double& x = history[j].adc[i].value;
      if (x > max) max = x;
      if (x < min) min = x;
    }
  }
  
  const double range = (max - min);
  const double gap = (range > 1e-5) ? 0.05* range : 1e-5;
  min -= gap;
  max += gap;
  
  for (size_t i = 0; i < N_INPUT_CHANNELS; ++i) {
    if (color == 10) color++; // skip white color
    
    if (! groupChannels->Find(i)->IsOn()) continue;
    
    TH1I* h = new TH1I("h", "h", 200, min, max);
    
    for (size_t j = 0; j < N; ++j) {
      h->Fill(history[j].adc[i].value);
    }
    
    h->SetTitle(history[N - 1].adc[i].channel->name);
    h->SetMarkerColor(color);
    h->SetLineColor(color);
    
    legend->AddEntry(h, "", "LP");
    hs->Add(h);
    
    color++;
  }
  
  legend->SetFillColor(0);
  //legend->SetBorderSize(0);
  //legend->SetTextFont(43);
  //legend->SetTextSize(20);
  
  // update legend size according to number of entries
  const double y1 = legend->GetY1();
  const double y2 = legend->GetY2();
  const double y1_new = y2 - (y2 - y1) * legend->GetNRows() / N_INPUT_CHANNELS;
  legend->SetY1(y1_new);
  
  TCanvas* c = canvasADCReadout->GetCanvas();
  //c->Clear();
  cleanCanvas(c);
  
  // no data to draw, exit
  //if (! hs->GetListOfGraphs()) return;
  
  hs->Draw("nostack");
  //hs->GetXaxis()->SetTitle("Time, s");
  //hs->GetYaxis()->SetTitle("Pixels Current");
  
  /*
  hs->GetXaxis()->SetNdivisions(20505);
  hs->GetXaxis()->SetLabelSize(0.035);
  hs->GetXaxis()->SetLabelFont(42);
  hs->GetXaxis()->SetTitleFont(42);
  
  hs->GetYaxis()->SetLabelSize(0.035);
  hs->GetYaxis()->SetLabelOffset(0.015);
  hs->GetYaxis()->SetLabelFont(42);
  hs->GetYaxis()->SetTitleFont(42);
  hs->GetYaxis()->SetTitleOffset(1.35);
  */
  //hs->GetYaxis()->SetRangeUser(minYAxis , maxYAxis);
  
  if (checkShowLegend->IsOn() && legend->GetNRows() > 0) legend->Draw();
  c->Update();
}

void MainFrame::DoDisplayReadoutData()
{
  if (comboDisplayType->GetSelected() == idGraph)
    UpdateADCGraphs();
  else
    UpdateADCHistograms();
}

void MainFrame::HelpAbout()
{
  // TODO: add svn revision
  
  new TGMsgBox(gClient->GetRoot(), this, "About",
    "SiPM Board Tester\n"
    "  builded on " __DATE__ " " __TIME__ "\n"
    "  with ROOT " ROOT_RELEASE "\n"
    "\n"
    "Author:\n"
    "  Anton Karneyeu (INR, Moscow)\n",
    kMBIconAsterisk, kMBOk, 0, kVerticalFrame, (kTextLeft | kTextTop));
}

void MainFrame::redraw(TGWindow* window)
{
  gClient->NeedRedraw(window);
  gSystem->ProcessEvents();
}

void MainFrame::CloseWindow()
{
  timer.Stop();
  DeleteWindow();
  gApplication->Terminate(0);
}

void MainFrame::setStatus(const char* status)
{
  statusBar->SetText(status);
  redraw(statusBar);
}
