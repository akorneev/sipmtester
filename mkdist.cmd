@echo off
set zip="c:\Program Files\7-Zip\7z.exe"

rem extract current svn revision
for /F %%i in ('svnversion -n') do set revision=%%i
set dist=sipm-tester-v%revision%
echo Creating distributive %dist% ...

rem cook arhive
mkdir %dist% || exit /B %ERRORLEVEL%
copy sipm-tester.exe %dist%\
copy *.profile %dist%\
copy .rootrc %dist%\
copy SDK\USBtoI2C32.dll %dist%\
%zip% a -tzip %dist%.zip %dist%\
rmdir /s /q %dist%

echo Distributive is ready: %dist%.zip
